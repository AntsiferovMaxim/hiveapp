import React from 'react';
import {RootStack} from './src';
import {configureStore} from '@/store';
import {Provider} from 'react-redux';

const store = configureStore();

export default class Index extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <RootStack/>
            </Provider>
        )
    }
}