import Realm from 'realm';

export const ClaimSchema = {
    name: 'Claim',
    primaryKey: 'id',
    properties: {
        id: 'string',
        type: 'string',
        data: 'string',
        verify: {type: 'bool', default: false}
    }
};

export const ProfileSchema = {
    name: 'Profile',
    primaryKey: 'id',
    properties: {
        id: 'string',
        name: 'string',
        createdAt: 'date',
        claims: {type: 'list', objectType: ClaimSchema.name, default: []}
    }
};

export const realm = new Realm({
    path: Realm.defaultPath,
    schema: [ClaimSchema, ProfileSchema],
    schemaVersion: 0,
    deleteRealmIfMigrationNeeded: true
});