import uuidv4 from 'uuid/v4';
import {realm, ProfileSchema, ClaimSchema} from './realm';
import {ClaimRealm} from './claim-realm';

export class ProfileRealm {
    static createProfile = (profile) => new Promise((resolve) => {
        realm.write(() => {
            const claims = profile.claims.map(item => realm.objectForPrimaryKey(ClaimSchema.name, item.id));

            const newProfile = realm.create(ProfileSchema.name, {
                id: uuidv4(),
                createdAt: new Date(),
                ...profile,
                claims
            });

            resolve(ProfileRealm.toPOJO(newProfile));
        });
    });

    static getProfiles = () => new Promise(resolve => {
        const profiles = realm.objects(ProfileSchema.name);

        resolve(profiles.map(ProfileRealm.toPOJO));
    });

    static deleteProfile = id => new Promise((resolve) => {
        realm.write(() => {
            const deletingProfile = realm.objectForPrimaryKey(ProfileSchema.name, id);

            realm.delete(deletingProfile);
            resolve(id)
        });
    });

    static updateProfile = profile => new Promise((resolve => {
        realm.write(() => {
            const oldProfile = realm.objectForPrimaryKey(ProfileSchema.name, profile.id);

            oldProfile.name = profile.name;
            oldProfile.claims = profile.claims;
            resolve(ProfileRealm.toPOJO(oldProfile));
        })
    }));

    static toPOJO = (profile) => ({
        id: profile.id,
        name: profile.name,
        createdAt: profile.createdAt,
        claims: profile.claims.map(ClaimRealm.toPOJO)
    })
}