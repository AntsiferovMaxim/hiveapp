import uuidv4 from 'uuid/v4';
import {realm, ClaimSchema} from './realm';

export class ClaimRealm {
    static createClaim = (claim) => new Promise((resolve) => {
        realm.write(() => {
            const newClaim = realm.create(ClaimSchema.name, {
                id: uuidv4(),
                ...claim,
            });

            resolve(ClaimRealm.toPOJO(newClaim))
        });
    });

    static getClaims = () => new Promise(resolve => {
        const claims = realm.objects(ClaimSchema.name);

        resolve(claims.map(ClaimRealm.toPOJO));
    });

    static deleteClaim = (id) => new Promise((resolve) => {
        realm.write(() => {
            const deletingClaim = realm.objectForPrimaryKey(ClaimSchema.name, id);

            realm.delete(deletingClaim);
            resolve(id);
        })
    });

    static toPOJO = (claim) => ({
        id: claim.id,
        type: claim.type,
        data: claim.data,
        verify: claim.verify
    });
}