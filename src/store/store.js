import {applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import {reducer} from './reducer';

export function configureStore(initialState = {}) {
    const middlewares = [
        thunk
    ];

    return createStore(
        reducer,
        initialState,
        applyMiddleware(...middlewares)
    );
}
