import {
    ADD_CLAIM,
    GET_CLAIMS,
    DELETE_CLAIM,
    claimTypes
} from './actions';

export function claimsReducers(state = [], {type, payload}) {
    switch (type) {
        case ADD_CLAIM: return [...state, payload];
        case GET_CLAIMS: return payload;
        case DELETE_CLAIM: return state.filter(item => item.id !== payload);
        default: return state;
    }
}

export const claimsSelector = state => state.claims;
export const emailsSelector = state => state.claims.filter(claim => claim.type === claimTypes.email);
export const phonesSelector = state => state.claims.filter(claim => claim.type === claimTypes.phone);