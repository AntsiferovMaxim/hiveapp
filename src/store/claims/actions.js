import {ClaimRealm} from '@/schemas/claim-realm';

export const ADD_CLAIM = 'ADD_CLAIM';
export const GET_CLAIMS = 'GET_CLAIMS';
export const DELETE_CLAIM = 'DELETE_CLAIM';

export const claimTypes = {
    location: 'LOCATION',
    email: 'EMAIL',
    phone: 'PHONE'
};

export const addClaim = (claim) => async dispatch => {
    const newClaim = await ClaimRealm.createClaim(claim);

    dispatch({
        type: ADD_CLAIM,
        payload: newClaim
    });
};

export const getClaims = () => async dispatch => {
    const claims = await ClaimRealm.getClaims();

    dispatch({
        type: GET_CLAIMS,
        payload: claims
    })
};

export const deleteClaim = (id) => async dispatch => {
    const deletedId = await ClaimRealm.deleteClaim(id);

    dispatch({
        type: DELETE_CLAIM,
        payload: deletedId
    })
};