import {combineReducers} from 'redux';

import {claimsReducers} from './claims/reducer';
import {profilesReducer} from './profiles/reducer';

export const reducer = combineReducers({
    claims: claimsReducers,
    profiles: profilesReducer
});