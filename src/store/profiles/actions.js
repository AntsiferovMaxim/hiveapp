import {ProfileRealm} from '@/schemas/profile-realm';

export const CREATE_PROFILE = 'CREATE_PROFILE';
export const GET_PROFILES = 'GET_PROFILES';
export const DELETE_PROFILE = 'DELETE_PROFILE';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

export const createProfile = profile => async dispatch => {
    const newProfile = await ProfileRealm.createProfile(profile);

    dispatch({
        type: CREATE_PROFILE,
        payload: newProfile
    })
};

export const getProfiles = () => async dispatch => {
    const profiles = await ProfileRealm.getProfiles();

    dispatch({
        type: GET_PROFILES,
        payload: profiles
    })
};

export const deleteProfile = (id) => async dispatch => {
    const deletedId = await ProfileRealm.deleteProfile(id);

    dispatch({
        type: DELETE_PROFILE,
        payload: deletedId
    })
};

export const updateProfile = (profile) => async dispatch => {
    const updatedProfile = await ProfileRealm.updateProfile(profile);

    dispatch({
        type: UPDATE_PROFILE,
        payload: updatedProfile
    })
};