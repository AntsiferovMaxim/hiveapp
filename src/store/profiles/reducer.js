import {
    GET_PROFILES,
    CREATE_PROFILE,
    DELETE_PROFILE,
    UPDATE_PROFILE
} from './actions';

export function profilesReducer(state = [], {type, payload}) {
    switch (type) {
        case GET_PROFILES: {
            return payload;
        }
        case CREATE_PROFILE: return [...state, payload];
        case UPDATE_PROFILE: {
            return state.map(item => item.id !== payload.id ? item : payload);
        }
        case DELETE_PROFILE: return state.filter(item => item.id !== payload);
        default: return state
    }
}

export const profilesSelector = state => state.profiles;
export const profileByIdSelector = (state, id) => state.profiles.find(item => item.id === id);