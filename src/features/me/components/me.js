import React from 'react';
import {Text, View, StyleSheet, StatusBar, ScrollView, SafeAreaView} from 'react-native';

import i18n from '@/utils/translate';
import {sizes, colors} from '@/ui/theme/index';
import {Header, NavCard, NavCardsList, SmallNavCard} from '@/ui/index';
import {HeaderLeftContainer, HeaderRightContainer} from '../atoms';

export class Me extends React.Component {
    handleCardClick = (to) => {
        this.props.navigation.navigate(to, {from: 'Me'});
    };

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <StatusBar
                        backgroundColor="white"
                        barStyle="dark-content"
                    />
                    <Header
                        LeftContainer={<HeaderLeftContainer onPress={this.handleCardClick}/>}
                        RightContainer={<HeaderRightContainer/>}
                    >
                        {i18n.t('home.me.header')}
                    </Header>
                    <ScrollView
                        style={meStyles.container}
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={meStyles.listContainer}>
                            <NavCardsList
                                title={i18n.t('home.me.essentials.title')}
                                description={i18n.t('home.me.essentials.description')}
                            >
                                {essentialsCard.map((item, index) => (
                                    <NavCard
                                        title={item.title}
                                        color={item.color}
                                        key={index}
                                        to={item.to}
                                        onPress={this.handleCardClick}
                                    >
                                        {item.description}
                                    </NavCard>
                                ))}
                            </NavCardsList>
                        </View>
                        <View style={meStyles.vaultContainer}>
                            <NavCardsList
                                title={i18n.t('home.me.vault.title')}
                                description={i18n.t('home.me.vault.description')}
                            >
                                {vaultCards.map((item, index) => (
                                    <SmallNavCard
                                        title={item.title}
                                        color={item.color}
                                        to={item.to}
                                        onPress={this.handleCardClick}
                                        key={index}
                                    />
                                ))}
                            </NavCardsList>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const meStyles = StyleSheet.create({
    container: {
      overflow: 'visible'
    },
    cardText: {
        color: '#495285',
        fontSize: sizes.navCardDescription
    },
    strong: {
        color: '#F18101'
    },
    listContainer: {
        paddingHorizontal: sizes.horizontalOffset,
        paddingTop: 25
    },
    vaultContainer: {
        marginHorizontal: sizes.horizontalOffset,
        paddingTop: 15,
        marginTop: 30,
        borderTopColor: colors.dividedBorder,
        borderTopWidth: 1,
    },
});

const essentialsCard = [
    {
        title: i18n.t('home.me.essentials.score'),
        description: <Text style={meStyles.cardText}>
            Current Score: <Text style={meStyles.strong}>570 (High risk)</Text> {/*TODO Add to strings*/}
        </Text>,
        color: '#FFB9F5',
        to: 'ReputationScore'
    }, {
        title: i18n.t('home.me.essentials.profiles'),
        description: <Text style={meStyles.cardText}>{i18n.t('home.me.essentials.profilesDescription')}</Text>,
        color: '#FFDEB9',
        to: 'MyProfiles'
    }
];

const vaultCards = [
    {
        title: i18n.t('home.me.vault.legalIdentity'),
        color: '#B9FFDD',
        to: 'LegalIdentity'
    }, {
        title: i18n.t('home.me.vault.emailAddresses'),
        color: '#E9FFB9',
        to: 'EmailAddresses'
    }, {
        title: i18n.t('home.me.vault.phoneNumbers'),
        to: 'PhoneNumbers',
        color: '#B9C5FF'
    },
];
