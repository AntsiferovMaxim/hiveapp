import { createStackNavigator } from 'react-navigation';

import {Me} from './components/me';
import {ReputationScore} from './features/reputation-score';
import {MyProfilesRoute} from './features/my-profiles';
import {Notifications} from './features/notifications';
import {LegalIdentity} from './features/legal-identity';
import {EmailRouter} from './features/email-addresses';
import {PhonesRouter} from './features/phone-numbers';

export default createStackNavigator({
    Me: {
        screen: Me,
    },
    ReputationScore: {
        screen: ReputationScore,
    },
    MyProfiles: {
        screen: MyProfilesRoute
    },
    Notifications: {
        screen: Notifications
    },
    LegalIdentity: {
        screen: LegalIdentity
    },
    EmailAddresses: {
        screen: EmailRouter
    },
    PhoneNumbers: {
        screen: PhonesRouter
    }
},  {
    initialRouteName: 'Me',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#FFFFFF' },
});
