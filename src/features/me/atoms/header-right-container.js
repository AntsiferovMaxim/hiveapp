import {Image, StyleSheet, View} from "react-native";
import React from "react";

const styles = StyleSheet.create({
    image: {
        borderRadius: 18,
        width: 36,
        height: 36
    }
});

export const HeaderRightContainer = () => (
    <View>
        <Image
            style={styles.image}
            source={{uri: link}}
        />
    </View>
);

const link = 'http://static2.uk.businessinsider.com/image/5a4baa69ec1ade46a0029ada-480/justin-timberlake.png';