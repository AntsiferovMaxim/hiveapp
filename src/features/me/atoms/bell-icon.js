import React from 'react';
import {Svg, G, Path, Defs, Circle, Use} from 'react-native-svg';

export const BellIcon = () => (
    <Svg width="20" height="24" viewBox="0 0 20 24">
        <Defs>
            <Path id="path-1" d="M11.7333333,18.366797 C11.7333333,19.9849415 10.4192,21.2982182 8.8,21.2982182 C7.1808,21.2982182 5.86666667,19.9849415 5.86666667,18.366797"/>
            <Path id="path-2" d="M2.93333333,6.64111256 C2.93333333,3.40335793 5.56013333,0.778270317 8.8,0.778270317 C12.0398667,0.778270317 14.6666667,3.40335793 14.6666667,6.64111256 L14.6666667,11.0382442 L17.5076,16.0260573 C17.7481333,16.5068103 17.5046667,16.9010865 16.9664,16.9010865 L0.6336,16.9010865 C0.0953333333,16.9010865 -0.148133333,16.5068103 0.0924,16.0260573 L2.93333333,11.0382442 L2.93333333,6.64111256 Z"/>
            <Circle id="path-3" cx="13" cy="3" r="3"/>
        </Defs>
        <G stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <G transform="translate(1.000000, 1.000000)">
                <G strokeLinecap="round" strokeLinejoin="round">
                    <Use stroke="#8A92B9" href="#path-1"/>
                    <Use stroke="#576090" href="#path-1"/>
                </G>
                <G strokeLinecap="round" strokeLinejoin="round">
                    <Use stroke="#8A92B9" href="#path-2"/>
                    <Use stroke="#576090" href="#path-2"/>
                </G>
                <G strokeLinecap="round" strokeLinejoin="round">
                    <Use fill="#6743DF" fillRule="evenodd" href="#path-3"/>
                    <Circle stroke="#FFFFFF" strokeWidth="2" cx="13" cy="3" r="4"/>
                </G>
            </G>
        </G>
    </Svg>
);