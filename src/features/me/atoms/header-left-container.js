import {TouchableOpacity} from "react-native";
import {BellIcon} from "./bell-icon";
import React from "react";

export const HeaderLeftContainer = ({onPress}) => (
    <TouchableOpacity onPress={() => onPress('Notifications')}>
        <BellIcon/>
    </TouchableOpacity>
);