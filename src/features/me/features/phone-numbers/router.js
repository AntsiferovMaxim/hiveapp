import {createStackNavigator} from 'react-navigation';

import {PhoneNumbersPage} from './';

export const PhonesRouter = createStackNavigator({
    PhoneNumbers: {
        screen: PhoneNumbersPage
    }
}, {
    initialRouteName: 'PhoneNumbers',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#FFFFFF' },
});