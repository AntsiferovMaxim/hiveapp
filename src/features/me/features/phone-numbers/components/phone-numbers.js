import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import i18n from '@/utils/translate';

import {Screen} from '@/ui';
import {PhoneNumberRow} from './phone-number-row';
import {phonesSelector} from '@/store/claims/reducer';
import {getClaims, deleteClaim} from '@/store/claims/actions';
import {colors, sizes} from "@/ui/theme";

const mapStateToProps = state => ({
    phones: phonesSelector(state)
});

const mapDispatchTpProps = {
    getClaims, deleteClaim
};

export class PhoneNumbersView extends Component {
    componentDidMount() {
        this.props.getClaims();
    }

    handleBack = () => this.props.navigation.navigate("Me");

    render() {
        const {phones, deleteClaim} = this.props;

        return (
            <Screen
                onBack={this.handleBack}
                fromTitle={i18n.t('home.me.header')}
                title={i18n.t('home.me.PhoneNumbers.header')}
            >
                <View style={[s.container, s.bordered]}>
                    {phones.map(item => (
                        <PhoneNumberRow
                            key={item.id} onDelete={deleteClaim}
                            id={item.id}
                            isVerify={item.verify}
                        >
                            {item.data}
                        </PhoneNumberRow>
                    ))}
                </View>
            </Screen>
        )
    }
}

const s = StyleSheet.create({
    container: {
        marginHorizontal: sizes.horizontalOffset,
    },
    bordered: {
        paddingBottom: 30,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    }
});

export const PhoneNumbersPage = connect(mapStateToProps, mapDispatchTpProps)(PhoneNumbersView);