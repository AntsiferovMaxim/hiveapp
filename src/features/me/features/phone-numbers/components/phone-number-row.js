import React from 'react';
import {StyleSheet} from 'react-native';
import {ClaimRow} from '@/ui';

import phoneIcon from '@/assets/images/claim-phone.png';

export const PhoneNumberRow = ({children, ...props}) => (
    <ClaimRow
        {...props}
        icon={phoneIcon}
        iconStyle={s.claimIcon}
    >
        {children}
    </ClaimRow>
);

const s = StyleSheet.create({
    claimIcon: {
        width: 22,
        height: 16
    }
});