import React from 'react';
import {View, Image, StyleSheet, Animated} from 'react-native';
import gradient from '../../assets/gradient.png';
import {fontSize} from '@/ui/theme';
import {Triangle} from '../atoms';

const size = 235;

export class ScoreCircle extends React.Component {
    render() {
        const {color, angle} = this.props;

        return (
            <View>
                <Image style={s.gradient} source={gradient}/>
                <Animated.View style={[s.backgroundCircle, {backgroundColor: color}]}/>
                <View style={s.whiteCircle}>
                    <Animated.Text style={[s.scoreText, {color}]}>470</Animated.Text>
                    <Animated.Text style={[s.scoreDescription, {color, borderColor: color}]}>Very High Risk</Animated.Text>
                </View>
                <Animated.View style={[s.triangleContainer, {
                    transform: [{rotate: angle}]
                }]}>
                    <View style={s.triangle}>
                        <Triangle color={color}/>
                    </View>
                </Animated.View>
            </View>
        )
    }
}

const s = StyleSheet.create({
    container: {
      position: 'relative'
    },
    triangleContainer: {
        width: size + 16,
        height: size + 16,
        position: 'absolute',
        // borderWidth: 1,
        // borderColor: colors.dividedBorder,
        transform: [{rotate: '230deg'}],
        zIndex: 1000
    },
    triangle: {
        position: 'absolute',
        width: 68,
        height: 67,
        // borderWidth: 1,
        // borderColor: colors.dividedBorder,
        left: -25,
        top: (size + 16) / 2 - 39,
        transform: [{rotate: '90deg'}],
        zIndex: 1001
    },
    gradient: {
        height: size,
        width: size + 16,
    },
    backgroundCircle: {
        width: size - 12,
        height: size - 12,
        backgroundColor: '#FF203D',
        position: 'absolute',
        borderRadius: size - 8,
        left: 14,
        top: 14
    },
    whiteCircle: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: size - 48,
        height: size - 48,
        backgroundColor: '#fff',
        position: 'absolute',
        borderRadius: size - 48,
        left: 32,
        top: 32,
        shadowColor: 'rgb(0,0,0)',
        shadowOffset: { width: 0, height: 12 },
        shadowOpacity: 0.25,
        shadowRadius: 22,
        elevation: 1,
        zIndex: 900
    },
    scoreText: {
        fontSize: fontSize(54),
        color: '#FF223E'
    },
    scoreDescription: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        color: '#FF223E',
        fontSize: fontSize(12),
        borderColor: '#FF223E',
        borderWidth: 1,
        borderRadius: 13
    }
});