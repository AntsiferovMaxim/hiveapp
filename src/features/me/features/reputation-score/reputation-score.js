import React from 'react';
import {View, StyleSheet, Text, SafeAreaView, ScrollView, Animated, Easing} from 'react-native';
import {sizes, fontSize} from '@/ui/theme';
import {Header, HeaderBackButton, NavCardsList, NavCard} from '../../../../ui';
import {ScoreCircle} from './ui';
import i18n from '@/utils/translate';

export class ReputationScore extends React.Component {
    constructor(props) {
        super(props);

        this.animatedColor = new Animated.Value(0);
    }

    handleBack = (from) => {
        this.props.navigation.goBack();
    };

    componentWillMount() {
        Animated.timing(this.animatedColor, {
            toValue: .9,
            duration: 2500,
            easing: Easing.inOut(Easing.ease)
        }).start();
    }

    render() {
        const params = this.props.navigation.state.params;
        const from = params && params.from;

        const color = this.animatedColor.interpolate({
            inputRange: [0, 1],
            outputRange: ['#D1192F', '#8EE10B']
        });

        const angle = this.animatedColor.interpolate({
            inputRange: [0, 1],
            outputRange: ['-50deg', '230deg']
        });

        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <Header
                        LeftContainer={<HeaderBackButton
                            onPress={this.handleBack}
                            from={from}
                        >
                            {from ? from : i18n.t('home.me.header')}
                        </HeaderBackButton>}
                    >
                        {i18n.t('home.me.ReputationScore.header')}
                    </Header>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={s.scoresContainer}>
                            <ScoreCircle color={color} angle={angle}/>
                            <View style={s.scoresDescriptionContainer}>
                                <View style={[s.scoresDescriptionItem, s.borderRight]}>
                                    <Text style={s.scoresDescriptionTitle}>
                                        {i18n.t('home.me.ReputationScore.scoreRange')}
                                    </Text>
                                    <Animated.Text style={[s.scoresRange, {color}]}>450 - 510</Animated.Text>
                                </View>
                                <View style={s.scoresDescriptionItem}>
                                    <Text style={s.scoresDescriptionTitle}>
                                        {i18n.t('home.me.ReputationScore.people')}
                                    </Text>
                                    <Text style={s.peopleRange}>3%</Text>
                                </View>
                            </View>
                        </View>
                        <View style={s.listContainer}>
                            <NavCardsList
                                title={i18n.t('home.me.ReputationScore.scoreTools')}
                                description={i18n.t('home.me.ReputationScore.scoreToolsDescription')}
                            >
                                {navCards.map((item, index) => (
                                    <NavCard
                                        title={item.title}
                                        color={item.color}
                                        key={index}
                                        to={item.to}
                                    >
                                        {item.description}
                                    </NavCard>
                                ))}
                            </NavCardsList>
                        </View>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    scroll: {
        marginBottom: 155
    },
    listContainer: {
        paddingHorizontal: 10,
        paddingTop: 25
    },
    scoresContainer: {
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 50
    },
    scoresDescriptionContainer: {
        flexDirection: 'row',
        marginTop: 50
    },
    scoresDescriptionItem: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 4
    },
    scoresDescriptionTitle: {
        fontSize: fontSize(14),
        color: '#A2A7AC'
    },
    scoresRange: {
        fontSize: fontSize(18),
        color: '#FF223E',
        fontWeight: '800',
        marginTop: 10
    },
    peopleRange: {
        fontSize: fontSize(18),
        marginTop: 10,
        fontWeight: '800',
        color: '#495285',
    },
    borderRight: {
        borderRightColor: '#E9ECF9',
        borderRightWidth: 1,
    },
    navText: {
        fontSize: sizes.navCardDescription,
        fontWeight: '600',
        color: '#495285'
    }
});

const navCards = [
    {
        title: i18n.t('home.me.ReputationScore.worksTitle'),
        description: <Text style={s.navText}>{i18n.t('home.me.ReputationScore.worksDescription')}</Text>,
        color: '#FFB9F5',
    }, {
        title: 'Reputation  Target',
        description: <Text style={s.navText}>4/10 {i18n.t('home.me.ReputationScore.targetDescription')}</Text>,
        color: '#FFDEB9'
    }
];