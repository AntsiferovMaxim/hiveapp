import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {fontSize} from "@/ui/theme/index";

export const EmailTitle = ({children}) => (
    <Text style={s.text}>{children}</Text>
);

const s = StyleSheet.create({
    text: {
        marginTop: 60,
        alignSelf: 'center',
        maxWidth: 230,
        fontSize: fontSize(22),
        color: '#323B45',
        fontWeight: '600',
        textAlign: 'center',
    }
});
