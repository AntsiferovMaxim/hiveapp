import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {fontSize} from "@/ui/theme/index";

export const EmailDescription = ({children}) => (
    <Text style={s.text}>{children}</Text>
);

const s = StyleSheet.create({
    text: {
        marginTop: 10,
        fontSize: fontSize(14),
        color: '#767FAB',
        textAlign: 'center'
    }
});
