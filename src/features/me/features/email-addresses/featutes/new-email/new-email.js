import React from 'react';
import {View, SafeAreaView, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {SubHeader, TextInput, PurpleButton} from '@/ui';
import i18n from '@/utils/translate';
import {sizes, fontSize} from '@/ui/theme';
import {addClaim} from '@/store/claims/actions';
import {EmailTitle, EmailDescription} from '../../atoms';

const mapDispatchToProps = {
    addClaim
};

export class NewEmailView extends React.Component {
    state = {
        email: ''
    };

    handleEmailInput = (text) => this.setState(() => ({email: text}));

    handleAddEmail = () => {
        this.props.addClaim({
            data: this.state.email,
            type: 'EMAIL',
            verify: true
        });
    };

    render() {
        const {email} = this.state;
        const {
            handleEmailInput,
            handleAddEmail
        } = this;

        return (
            <SafeAreaView style={{flex: 1}}>
                <SubHeader onBack={this.props.navigation.goBack}>
                    {i18n.t('home.me.EmailAddresses.NewEmail.header')}
                </SubHeader>
                <View style={[{flex: 1, marginHorizontal: 10}]}>
                    <EmailTitle>
                        {i18n.t('home.me.EmailAddresses.NewEmail.title')}
                    </EmailTitle>
                    <EmailDescription>
                        {i18n.t('home.me.EmailAddresses.NewEmail.description')}
                    </EmailDescription>
                    <View style={s.form}>
                        <TextInput
                            placeholder="heaven8@outlook.com"
                            textContentType="emailAddress"
                            value={email}
                            onChangeText={handleEmailInput}
                        >
                            {i18n.t('home.me.EmailAddresses.NewEmail.email')}
                        </TextInput>
                        <View style={s.buttonContainer}>
                            <PurpleButton
                                onPress={handleAddEmail}
                            >
                                {i18n.t('home.me.EmailAddresses.NewEmail.verify')}
                            </PurpleButton>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    buttonContainer: {
        marginTop: 25,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    title: {
        marginTop: 60,
        alignSelf: 'center',
        maxWidth: 230,
        fontSize: fontSize(22),
        color: '#323B45',
        fontWeight: '600',
        textAlign: 'center',
    },
    description: {
        marginTop: 10,
        fontSize: fontSize(14),
        color: '#767FAB',
        textAlign: 'center'
    },
    form: {
        marginTop: 40,
        marginHorizontal: sizes.horizontalOffset,
        flexDirection: 'column',
    }
});

export const NewEmailPage = connect(null, mapDispatchToProps)(NewEmailView);
