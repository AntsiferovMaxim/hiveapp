import React from 'react';
import {View, SafeAreaView, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native';
import {SubHeader} from '@/ui/index';
import {NumDisplay, Keyboard} from './ui';
import i18n from "@/utils/translate";
import {fontSize} from "@/ui/theme";
import {EmailTitle, EmailDescription} from '../../atoms';

export class VerifyEmail extends React.Component {
    state = {
        code: []
    };

    handleKeyboard = (code) => {
        this.setState({code})
    };

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <SubHeader onBack={this.props.navigation.goBack} rightText={i18n.t('home.me.EmailAddresses.VerifyEmail.done')}>
                    {i18n.t('home.me.EmailAddresses.VerifyEmail.header')}
                </SubHeader>
                <View style={[{flex: 1, marginHorizontal: 10}]}>
                    <EmailTitle>
                        {i18n.t('home.me.EmailAddresses.VerifyEmail.check')}
                    </EmailTitle>
                    <EmailDescription>
                        {i18n.t('home.me.EmailAddresses.VerifyEmail.send')}
                    </EmailDescription>
                    <Text style={s.email}>antsiferovmaximv@gmail.com</Text>
                    <View style={s.codeDisplay}>
                        <NumDisplay numbers={this.state.code.concat('')}/>
                        <Text style={s.displayDescription}>
                            {i18n.t('home.me.EmailAddresses.VerifyEmail.enter')}
                        </Text>
                    </View>
                    <View style={s.centeredBlock}>
                        <Text style={s.verifyDescription}>
                            {i18n.t('home.me.EmailAddresses.VerifyEmail.description')}
                        </Text>
                        <TouchableWithoutFeedback>
                            <Text style={s.textButton}>{i18n.t('home.me.EmailAddresses.VerifyEmail.resend')}</Text>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
                <View style={s.keyboardContainer}>
                    <Keyboard max={6} onChange={this.handleKeyboard}/>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    email: {
        marginTop: 5,
        fontWeight: '600',
        fontSize: fontSize(16),
        color: '#323B45',
        textAlign: 'center'
    },
    codeDisplay: {
        marginHorizontal: 30,
        marginTop: 50
    },
    displayDescription: {
        marginTop: 6,
        fontSize: fontSize(12),
        color: '#767FAB',
        textAlign: 'center'
    },
    textButton: {
        fontSize: fontSize(12),
        fontWeight: '600',
        color: '#6743DF'
    },
    verifyDescription: {
        fontSize: fontSize(12),
        color: '#767FAB',
        textAlign: 'center',
        marginBottom: 8
    },
    centeredBlock: {
        marginTop: 24,
        alignItems: 'center',
        marginHorizontal: 20
    },
    keyboardContainer: {
        position: 'absolute',
        bottom: 0,
        left: 0
    }
});

