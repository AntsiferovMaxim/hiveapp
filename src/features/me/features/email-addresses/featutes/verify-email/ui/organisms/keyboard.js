import React from 'react';
import {View, StyleSheet} from 'react-native';
import {KeyboardCell as Cell} from '../atoms';

export class Keyboard extends React.Component {
    state = {
        code: []
    };

    update = () => {
        this.props.onChange && this.props.onChange(this.state.code)
    };

    addNumber = (number) => {
        if (this.state.code.length < this.props.max) {
            this.setState({
                code: [...this.state.code, number]
            }, this.update)
        }
    };

    handleDelete = () => {
        this.setState(({code}) => ({
            code: code.slice(0, code.length - 1)
        }), this.update)
    };

    handlePress = (num) => () => this.addNumber(num);

    render() {
        return (
            <View style={s.container}>
                <View style={s.row}>
                    <Cell onPress={this.handlePress(1)}>1</Cell>
                    <Cell onPress={this.handlePress(2)} middle>2</Cell>
                    <Cell onPress={this.handlePress(3)}>3</Cell>
                </View>
                <View style={[s.row, s.marginTop]}>
                    <Cell onPress={this.handlePress(4)}>4</Cell>
                    <Cell onPress={this.handlePress(5)} middle>5</Cell>
                    <Cell onPress={this.handlePress(6)}>6</Cell>
                </View>
                <View style={[s.row, s.marginTop]}>
                    <Cell onPress={this.handlePress(7)}>7</Cell>
                    <Cell onPress={this.handlePress(8)} middle>8</Cell>
                    <Cell onPress={this.handlePress(9)}>9</Cell>
                </View>
                <View style={[s.row, s.marginTop]}>
                    <Cell.Empty/>
                    <Cell middle>0</Cell>
                    <Cell.Delete onPress={this.handleDelete}/>
                </View>
            </View>
        )
    }
}

const s = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(171, 179, 189, 0.6)'
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    text: {
        fontSize: 28
    },
    marginTop: {
        marginTop: 1
    },
});