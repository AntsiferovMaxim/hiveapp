import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export const NumDisplay = ({numbers}) => (
    <View style={s.container}>
        {[0, 1, 2, 3, 4, 5].map(num => (
            <View key={num} style={[s.numContainer, num === 2 && s.marginRight, num === 3 && s.marginLeft]}>
                <Text style={s.number}>{numbers[num] || ''}</Text>
            </View>
        ))}
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    numContainer: {
        width: 40,
        height: 58,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 6,
        backgroundColor: 'rgba(103, 67, 223, .3)'
    },
    number: {
        color: '#495285',
        fontSize: 17,
        fontWeight: "900"
    },
    marginLeft: {
        marginLeft: 6
    },
    marginRight: {
        marginRight: 6
    }
});