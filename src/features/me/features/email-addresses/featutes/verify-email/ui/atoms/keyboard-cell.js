import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet, Dimensions, Image} from 'react-native';
import deleteIcon from '../../assets/keyboard-delete-icon.png';

const {width} = Dimensions.get('window');

export const KeyboardCell = ({children, onPress, middle}) => (
    <TouchableOpacity style={[s.cell, middle && s.middle]} onPress={onPress}>
        <Text style={s.text}>{children}</Text>
    </TouchableOpacity>
);

const Empty = () => (
    <View style={[s.cell, s.empty]}/>
);

const Delete = ({onPress}) => (
    <TouchableOpacity style={[s.cell, s.empty]} onPress={onPress}>
        <Image style={s.deleteIcon} source={deleteIcon}/>
    </TouchableOpacity>
);

KeyboardCell.Empty = Empty;
KeyboardCell.Delete = Delete;


const s = StyleSheet.create({
    cell: {
        width: width / 3 - .67,
        height: Math.floor(width / 3 * 0.432),
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    empty: {
        backgroundColor: 'rgba(0, 0, 0, 0)'
    },
    text: {
        fontSize: 28
    },
    middle: {
        marginHorizontal: 1
    },
    deleteIcon: {
        width: 22.88,
        height: 17
    }
});
