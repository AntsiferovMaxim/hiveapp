import React from 'react';
import {StyleSheet} from 'react-native';
import {ClaimRow} from '@/ui';

import emailIcon from '../assets/email-icon.png';

export const EmailAddress = ({children, ...props}) => (
    <ClaimRow
        {...props}
        icon={emailIcon}
        iconStyle={s.claimIcon}
    >
        {children}
    </ClaimRow>
);

const s = StyleSheet.create({
    claimIcon: {
        width: 22,
        height: 16
    },
});
