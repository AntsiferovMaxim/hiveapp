import React from 'react';
import {connect} from 'react-redux';
import {View, StyleSheet} from 'react-native';
import i18n from '@/utils/translate';

import {sizes, colors} from '@/ui/theme/index';
import {MainTitleWithAddButton, Screen} from "@/ui/index";
import {EmailAddress} from '../molecules';
import {emailsSelector} from '@/store/claims/reducer';
import {getClaims, deleteClaim} from '@/store/claims/actions';

const mapStateToProps = state => ({
    emails: emailsSelector(state)
});

const mapDispatchToProps = {
    getClaims, deleteClaim
};

export class EmailAddressesView extends React.Component {
    componentDidMount() {
        this.props.getClaims();
    }

    handleBack = () => {
        this.props.navigation.navigate('Me')
    };

    onAddEmail = () => {
        this.props.navigation.navigate('NewEmail')
    };

    render() {
        const params = this.props.navigation.state.params;
        const from = params && params.from;
        const {emails, deleteClaim} = this.props;

        return (
            <Screen
                onBack={this.handleBack}
                from={from}
                fromTitle={from ? from : i18n.t('home.me.header')}
                title={i18n.t('home.me.EmailAddresses.header')}
            >
                <View style={[s.container, s.bordered]}>
                    {emails.map(item => (
                        <EmailAddress
                            key={item.id} onDelete={deleteClaim}
                            id={item.id}
                            isVerify={item.verify}
                        >
                            {item.data}
                        </EmailAddress>
                    ))}
                </View>
                <View style={s.container}>
                    <MainTitleWithAddButton
                        purple
                        onAdd={this.onAddEmail}
                    >
                        {i18n.t('home.me.EmailAddresses.addNew')}
                    </MainTitleWithAddButton>
                </View>
            </Screen>
        )
    }
}

export const EmailAddressesPage = connect(mapStateToProps, mapDispatchToProps)(EmailAddressesView);

const s = StyleSheet.create({
    container: {
        marginHorizontal: sizes.horizontalOffset,
    },
    bordered: {
        paddingBottom: 30,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    }
});