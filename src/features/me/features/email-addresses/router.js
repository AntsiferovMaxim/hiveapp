import {createStackNavigator} from 'react-navigation';

import {EmailAddressesPage} from "./components/email-addresses";
import {NewEmailPage} from './featutes/new-email';
import {VerifyEmail} from './featutes/verify-email';

export const EmailRouter = createStackNavigator({
    EmailAddresses: {
        screen: EmailAddressesPage
    },
    NewEmail: {
        screen: NewEmailPage
    },
    VerifyEmail: {
        screen: VerifyEmail
    }
}, {
    initialRouteName: 'EmailAddresses',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#FFFFFF' },
});
