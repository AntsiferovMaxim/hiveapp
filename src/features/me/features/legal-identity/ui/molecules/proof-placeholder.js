import React from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';
import i18n from '@/utils/translate';

import placeholderImage from '../../assets/proof-placeholder.png';
import placeholderArrow from '../../assets/proof-arrow.png';
import {fontSize} from '@/ui/theme';

export const ProofPlaceholder = () => (
    <View style={s.container}>
        <View style={s.imageContainer}>
            <Image style={s.imagePlaceholder} source={placeholderImage}/>
            <Image style={s.arrowImage} source={placeholderArrow}/>
        </View>
        <Text style={s.title}>{i18n.t('home.me.LegalIdentity.proofTitle')}</Text>
        <Text style={s.description}>
            {i18n.t('home.me.LegalIdentity.proofDescription')}
        </Text>
    </View>
);

const s = StyleSheet.create({
    container: {
        marginTop: 30,
        flexDirection: 'column',
        alignItems: 'center'
    },
    imageContainer: {
        position: 'relative',
        overflow: 'visible'
    },
    arrowImage: {
        position: 'absolute',
        width: 64,
        height: 50,
        right: -80
    },
    imagePlaceholder: {
        width: 64,
        height: 60
    },
    title: {
        marginTop: 23,
        fontSize: fontSize(16),
        fontWeight: '600',
        color: '#323B45',
        opacity: .8
    },
    description: {
        marginTop: 13,
        fontSize: fontSize(14),
        color: '#767FAB',
        opacity: .8,
        textAlign: 'center',
        maxWidth: 320
    }
});