import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {colors, fontSize} from '@/ui/theme';

export const ClaimItem = ({title, children}) => (
    <View style={s.container}>
        <Text style={s.title}>{title}</Text>
        <Text style={s.data}>{children}</Text>
    </View>
);

const s = StyleSheet.create({
    container: {
        marginTop: 18,
        marginLeft: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder,
        paddingBottom: 3
    },
    title: {
        fontSize: fontSize(12),
        color: '#767FAB',
        letterSpacing: .3
    },
    data: {
        marginTop: 6,
        fontSize: fontSize(16),
        fontWeight: '500',
        color: '#495285',
        letterSpacing: .4
    }
});