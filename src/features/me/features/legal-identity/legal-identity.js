import React from 'react';
import {StatusBar, View, StyleSheet, SafeAreaView} from 'react-native';

import {Header, HeaderBackButton, MainTitleWithAddButton} from '@/ui';
import {sizes} from '@/ui/theme';
import {ClaimItem, ProofPlaceholder} from './ui';
import i18n from "@/utils/translate";

export class LegalIdentity extends React.Component {
    handleBack = (from) => {
        this.props.navigation.navigate('Me')
    };

    render() {
        const params = this.props.navigation.state.params;
        const from = params && params.from;

        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <StatusBar
                        backgroundColor="#fff"
                        barStyle="dark-content"
                    />
                    <Header
                        LeftContainer={<HeaderBackButton onPress={this.handleBack}
                                                         from={from}>{from ? from : i18n.t('home.me.header')}</HeaderBackButton>}
                    >
                        {i18n.t('home.me.LegalIdentity.header')}
                    </Header>
                    <View style={s.container}>
                        {claims.map((item, index) => (
                            <ClaimItem title={item.title.toUpperCase()} key={index}>
                                {item.data}
                            </ClaimItem>
                        ))}
                    </View>
                    <View style={s.container}>
                        <MainTitleWithAddButton>
                            Proof of identity
                        </MainTitleWithAddButton>
                        <ProofPlaceholder/>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    container: {
        marginHorizontal: sizes.horizontalOffset,
        marginTop: 10
    }
});

const claims = [
    {
        title: i18n.t('home.me.LegalIdentity.firstName'),
        data: 'Johanna'
    },
    {
        title: i18n.t('home.me.LegalIdentity.lastName'),
        data: 'Vishinsky'
    },
    {
        title: i18n.t('home.me.LegalIdentity.dob'),
        data: 'Dec 18, 1988'
    }
];
