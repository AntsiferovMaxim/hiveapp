import React from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {colors, fontSize} from "@/ui/theme/index";

 export const PanelClaimItem = ({icon, children, iconSize, onPress, id, isSelected}) => (
    <TouchableOpacity
        style={[s.claimContainer, isSelected && s.selected]}
        onPress={() => onPress(id)}
        disabled={isSelected}
        activeOpacity={0.3}
    >
        <View style={s.claimIconContainer}>
            <Image style={iconSize} source={icon}/>
        </View>
        <View style={s.claimContent}>
            <Text style={s.claimContentText}>{children}</Text>
        </View>
    </TouchableOpacity>
);

const s = StyleSheet.create({
    claimContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    selected: {
        opacity: .3
    },
    claimIconContainer: {
        width: 24,
        height: 24,
        marginRight: 16
    },
    claimContent: {
        flex: 1,
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder,
    },
    claimContentText: {
        fontSize: fontSize(14),
        color: '#495285',
        fontWeight: '500'
    },
});