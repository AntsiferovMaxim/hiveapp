import React from 'react';
import {Animated, Dimensions, StyleSheet, View, Platform} from 'react-native';
import Interactable from 'react-native-interactable';
import {isIphoneX} from "react-native-iphone-x-helper";

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

const SNAP_POINTS = [
    isIphoneX() ? 70 : 0,
    SCREEN_HEIGHT * 0.5,
    SCREEN_HEIGHT * 0.8
];
const PANEL_TOUCHABLE_AREA_HEIGHT = 20;

export class BottomPanel extends React.Component {
    constructor(props) {
        super(props);

        const points = props.snapPoints ? props.snapPoints : SNAP_POINTS;

        this.SNAP_POINTS = points.map(point => ({y: point}));
        this._deltaY = new Animated.Value(SCREEN_HEIGHT * 0.8);
    }

    render() {

        const SNAP_POINT_TOP = this.SNAP_POINTS[0].y;
        const SNAP_POINT_BOTTOM = this.SNAP_POINTS[this.SNAP_POINTS.length - 1].y;

        const height = this._deltaY.interpolate({
            inputRange: [
                SNAP_POINT_TOP,
                SNAP_POINT_BOTTOM
            ],
            outputRange: [
                SCREEN_HEIGHT - PANEL_TOUCHABLE_AREA_HEIGHT - (isIphoneX() ? 70 : 0),
                SCREEN_HEIGHT * 0.2 - PANEL_TOUCHABLE_AREA_HEIGHT - (isIphoneX() ? 20 : 0)
            ]
        });

        return (
            <Interactable.View
                verticalOnly={true}
                animatedValueY={this._deltaY}
                snapPoints={this.SNAP_POINTS}
                initialPosition={{y: SNAP_POINT_BOTTOM}}
                boundaries={{bottom: SNAP_POINT_BOTTOM, top: SNAP_POINT_TOP, bounce: 2}}
                friction={0.3}
                style={s.panel}
            >
                <View style={s.panelTouchableArea}/>
                <Animated.View style={{height}}>
                    {this.props.children}
                </Animated.View>
            </Interactable.View>
        )
    }
}

const s = StyleSheet.create({
    panel: {
        position: 'absolute',
        width: SCREEN_WIDTH,
        height: SCREEN_HEIGHT,
        backgroundColor: '#ffffff',
        left: 0,
        bottom: 0,
        paddingHorizontal: 16,
        shadowColor: '#E9EAF5',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.30,
        shadowRadius: 10,
        elevation: 3,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        zIndex: 1000
    },
    panelTouchableArea: {
        marginTop: 8,
        marginBottom: 8,
        alignSelf: 'center',
        width: 40,
        height: 4,
        borderRadius: 2,
        backgroundColor: '#767FAB'
    },
});
