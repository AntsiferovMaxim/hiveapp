import React from 'react';
import {View, StyleSheet, Text, Image, Animated, TouchableWithoutFeedback} from 'react-native';

import cancelIcon from '@/assets/images/cancel-icon.png';
import {fontSize} from "@/ui/theme/index";

export class ClaimItem extends React.Component {
    constructor() {
        super();

        this.containerHeight = null;
    }

    onContainerLayout = (event) => {
        this.containerHeight = event.nativeEvent.layout.height;
    };

    handleDelete = () => {
        this.props.onDelete(this.props.id);
    };

    render() {
        const {title, children, icon, iconSize} = this.props;

        return (
            <Animated.View onLayout={this.onContainerLayout} style={[s.container]}>
                <View style={s.topContainer}>
                    <Text style={s.title}>{title}</Text>
                    <TouchableWithoutFeedback onPress={this.handleDelete}>
                        <Image style={s.icon} source={cancelIcon}/>
                    </TouchableWithoutFeedback>
                </View>
                <View style={s.bottomContainer}>
                    <Image style={iconSize} source={icon}/>
                    <Text style={s.content}>{children}</Text>
                </View>
            </Animated.View>
        )
    }
}

const s = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingTop: 14,
        paddingHorizontal: 14,
        paddingBottom: 10,
        marginHorizontal: 16,
        marginTop: 12,
        marginBottom: 12,
        borderRadius: 8,
        backgroundColor: 'white',
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: { width: 0, height: -1 },
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 10,
    },
    icon: {
        width: 24,
        height: 24,
        opacity: .3
    },
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    bottomContainer: {
        marginTop: 6,
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontSize: fontSize(17),
        color: '#323B45',
        fontWeight: '800',
        letterSpacing: 0.27
    },
    content: {
        fontSize: fontSize(14),
        color: '#495285',
        fontWeight: '500',
        marginLeft: 10
    }
});
