import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {SafeAreaView, StyleSheet, Text, ScrollView, Dimensions, Platform} from 'react-native';
import {isIphoneX} from 'react-native-iphone-x-helper';

import {SubHeader, HeaderInput, StatusBar} from '@/ui/index';
import {fontSize} from "@/ui/theme/index";
import {BottomPanel, PanelClaimItem, ClaimItem} from './ui/index';
import {claimTypes, getClaims} from '@/store/claims/actions';
import {claimsSelector} from '@/store/claims/reducer';
import {createProfile, updateProfile} from '@/store/profiles/actions';

import claimEmailIcon from '@/assets/images/claim-email.png';
import claimPhoneIcon from '@/assets/images/claim-phone.png';
import claimLocationIcon from '@/assets/images/claim-location.png';
import {profileByIdSelector} from "@/store/profiles/reducer";

const {height: SCREEN_HEIGHT} = Dimensions.get('window');

const SNAP_POINT_BOTTOM = SCREEN_HEIGHT * 0.8;
const SNAP_POINT_MIDDLE = SCREEN_HEIGHT * 0.5;
const SNAP_POINT_TOP = isIphoneX() ? 70 : 0;

const claimIcons = {
    [claimTypes.phone]: {
        source: claimPhoneIcon,
        size: {
            width: 23,
            height: 23,
        }
    },
    [claimTypes.email]: {
        source: claimEmailIcon,
        size: {
            width: 22,
            height: 16,
        }
    },
    [claimTypes.location]: {
        source: claimLocationIcon,
        size: {
            width: 17,
            height: 24,
        }
    },
};

const claimsTitles = {
    [claimTypes.phone]: 'Phone Number',
    [claimTypes.email]: 'Email Address',
    [claimTypes.location]: 'Street Address',
};

const mapStateToProps = (state, props) => ({
    claims: claimsSelector(state),
    profile: profileByIdSelector(
        state,
        props.navigation.state.params ?
            props.navigation.state.params.profileId :
            undefined
    )
});

const mapDispatchToProps = {
    getClaims,
    createProfile,
    updateProfile
};

export class CreateProfileView extends React.Component {
    state = {
        headerHeight: null,
        selected: [],
        profileName: '',
        profile: null
    };

    componentDidMount() {
        if (this.isProfileEdit()) {
            this.setState(() => ({
                profileName: this.props.profile.name,
                selected: this.props.profile.claims
            }));
        }

        this.props.getClaims();
    }

    groupByType = () => {
        return this.props.claims.reduce((acc, cur) => {
            acc[cur.type] ? acc[cur.type].push(cur) : acc[cur.type] = new Array(cur);

            return acc;
        }, []);
    };

    claimIsSelected = (id) => {
        for (let item of this.state.selected) {
            if (item.id === id) return true
        }

        return false;
    };

    isProfileEdit = () => {
        return !!this.getProfileId()
    };

    getProfileId = () => {
        return this.props.navigation.state.params &&
            this.props.navigation.state.params.profileId;
    };

    onHeaderLayout = (event) => {
        const height = event.nativeEvent.layout.height;

        this.setState(() => ({
            headerHeight: height
        }));
    };

    handleChangeName = (text) => this.setState(() => ({profileName: text}));

    handleClearName = () => this.handleChangeName('');

    handleCreateProfile = () => {
        this.isProfileEdit() ?
            this.props.updateProfile({
                id: this.getProfileId(),
                name: this.state.profileName,
                claims: this.state.selected
            }) :
            this.props.createProfile({
                name: this.state.profileName,
                claims: this.state.selected
            })
    };

    handleAddClaim = (id) => this.setState(state => ({
        selected: [
            ...state.selected,
            this.props.claims.filter(item => item.id === id)[0]
        ]
    }));

    handleDeleteClaim = (id) => this.setState(state => ({
        selected: state.selected.filter(item => item.id !== id)
    }));

    renderHeader = () => {
        const profileName = this.state.profileName.length > 0 ? this.state.profileName : 'Untiled';

        return (
            <>
                <StatusBar/>
                <SubHeader
                    backText="My profile"
                    onBack={this.props.navigation.goBack}
                    rightText="Done"
                    onLayout={this.onHeaderLayout}
                    onRightButton={this.handleCreateProfile}
                    bottomContainer={
                        <HeaderInput
                            placeholder={profileName}
                            onChange={this.handleChangeName}
                            onClear={this.handleClearName}
                            value={this.state.profileName}
                        />
                    }
                >
                    {profileName}
                </SubHeader>
            </>
        )
    };

    renderPanelClaims = (title, claims) => (claims &&
        <Fragment>
            <Text style={s.panelTitle}>{title}</Text>
            {claims.map(item => (
                <PanelClaimItem
                    key={item.id}
                    icon={claimIcons[item.type].source}
                    iconSize={claimIcons[item.type].size}
                    id={item.id}
                    isSelected={this.claimIsSelected(item.id)}
                    onPress={this.handleAddClaim}
                >
                    {item.data}
                </PanelClaimItem>
            ))}
        </Fragment>
    );

    renderSelectedClaims = () => this.state.selected.map(item => (
        <ClaimItem
            key={item.id}
            id={item.id}
            title={claimsTitles[item.type]}
            icon={claimIcons[item.type].source}
            iconSize={claimIcons[item.type].size}
            onDelete={this.handleDeleteClaim}
        >
            {item.data}
        </ClaimItem>
    ));

    render() {
        const containerHeight = {
            maxHeight: SCREEN_HEIGHT - this.state.headerHeight - (SCREEN_HEIGHT - SNAP_POINT_BOTTOM) + Platform.select({
                ios: -40,
                android: StatusBar.currentHeight
            }),
        };
        const {
            [claimTypes.phone]: phone,
            [claimTypes.email]: email,
            [claimTypes.location]: location
        } = this.groupByType();

        return (
            <SafeAreaView style={{flex: 1, position: 'relative'}}>
                {this.renderHeader()}
                <ScrollView
                    style={[s.container, containerHeight]}
                >
                    {this.renderSelectedClaims()}
                </ScrollView>
                <BottomPanel
                    snapPoints={[SNAP_POINT_TOP, SNAP_POINT_MIDDLE, SNAP_POINT_BOTTOM]}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        {this.renderPanelClaims('Email Addresses', email)}
                        {this.renderPanelClaims('Phone Numbers', phone)}
                        {this.renderPanelClaims('Street Addresses', location)}
                    </ScrollView>
                </BottomPanel>
            </SafeAreaView>
        )
    }
}

export const CreateProfile = connect(mapStateToProps, mapDispatchToProps)(CreateProfileView);

const s = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    panelTitle: {
        marginTop: 24,
        marginBottom: 6,
        fontSize: fontSize(17),
        color: '#323B45',
        fontWeight: '700'
    },
});
