import { createStackNavigator } from 'react-navigation';

import {MyProfiles} from './my-profiles';
import {CreateProfile} from './features/create-profile';

export default createStackNavigator({
    MyProfiles: {
        screen: MyProfiles
    },
    CreateProfile: {
        screen: CreateProfile
    }
}, {
    initialRouteName: 'MyProfiles',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#FFFFFF' },
});