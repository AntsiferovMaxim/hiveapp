import React from 'react';
import {View, StyleSheet, ScrollView, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import moment from 'moment';

import {sizes} from '@/ui/theme';
import {Header, HeaderBackButton, HeaderAddButton} from '@/ui';
import {ProfileCard} from './molecules';
import i18n from '@/utils/translate';
import {profilesSelector, profileByIdSelector} from '@/store/profiles/reducer';
import {getProfiles, deleteProfile} from '@/store/profiles/actions';

const mapStateTpProps = state => ({
    profiles: profilesSelector(state)
});

const mapDispatchToProps = {
    getProfiles,
    deleteProfile
};

export class MyProfilesView extends React.Component {
    componentDidMount() {
        this.props.getProfiles();
    }


    handleBack = (from) => {
        this.props.navigation.navigate('Me');
    };

    handleAdd = () => {
        this.props.navigation.navigate('CreateProfile')
    };

    handleProfileClick = (id) => {
        console.log(id);
        this.props.navigation.navigate('CreateProfile', {profileId: id})
    };

    render() {
        const params = this.props.navigation.state.params;
        const from = params && params.from;
        const {profiles, deleteProfile} = this.props;

        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <Header
                        LeftContainer={<HeaderBackButton onPress={this.handleBack}
                                                         from={from}>{from ? from : i18n.t('home.me.header')}</HeaderBackButton>}
                        RightContainer={<HeaderAddButton onPress={this.handleAdd}/>}
                    >
                        {i18n.t('home.me.MyProfiles.header')}
                    </Header>
                    <ScrollView style={s.cardsContainer}>
                        {profiles.map(profile => (
                            <ProfileCard
                                onClick={() => this.handleProfileClick(profile.id)}
                                onMore={() => deleteProfile(profile.id)}
                                key={profile.id}
                                date={`${i18n.t('home.me.MyProfiles.created')}: ${moment(profile.createdAt).format("MMMM D, YYYY")}`}
                            >
                                {profile.name}
                            </ProfileCard>
                        ))}
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

export const MyProfiles = connect(mapStateTpProps, mapDispatchToProps)(MyProfilesView);

const s = StyleSheet.create({
    cardsContainer: {
        paddingVertical: 30,
        paddingHorizontal: sizes.horizontalOffset
    }
});
