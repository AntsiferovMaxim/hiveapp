import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Image, TouchableWithoutFeedback} from 'react-native';

import {fontSize} from '@/ui/theme/index';
import moreIcon from '@/assets/images/more-icon.png';

export const ProfileCard = ({children, date, onMore, onClick}) => (
    <TouchableWithoutFeedback onPress={onClick}>
        <View style={s.cardContainer}>
            <View style={s.cardHeader}>
                <Text style={s.cardTitle}>{children}</Text>
                <TouchableOpacity onPress={onMore}>
                    <Image style={s.moreIcon} source={moreIcon}/>
                </TouchableOpacity>
            </View>
            <Text style={s.cardDate}>{date}</Text>
        </View>
    </TouchableWithoutFeedback>
);

const s = StyleSheet.create({
    cardContainer: {
        backgroundColor: '#fff',
        paddingVertical: 14,
        paddingHorizontal: 16,
        marginBottom: 15,
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: { width: 0, height: -1 },
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 1,
        borderRadius: 6,
        margin: 1
    },
    cardHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardTitle: {
        fontSize: fontSize(17),
        color: '#495285',
        fontWeight: '900'
    },
    cardDate: {
        fontSize: fontSize(14),
        fontWeight: '300',
        color: '#495285',
        marginTop: 6
    },
    moreIcon: {
        width: 24,
        height: 24
    }
});