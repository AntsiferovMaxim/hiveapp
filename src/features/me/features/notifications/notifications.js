import React from 'react';
import {View, StyleSheet, ScrollView, StatusBar, SafeAreaView} from 'react-native';
import {styles, sizes, colors} from '@/ui/theme';
import {Header, HeaderBackButton, MainTitle} from '@/ui';
import {AddCard, MessageItem} from './ui';
import i18n from '@/utils/translate';

export class Notifications extends React.Component {
    handleBack = (from) => {
        this.props.navigation.goBack()
    };

    render() {
        const params = this.props.navigation.state.params;
        const from = params && params.from;

        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <StatusBar
                        barStyle="dark-content"
                    />
                    <Header
                        LeftContainer={<HeaderBackButton onPress={this.handleBack}
                                                         from={from}>{from ? from : i18n.t('home.me.header')}</HeaderBackButton>}
                    >
                        {i18n.t('home.me.Notifications.header')}
                    </Header>
                    <ScrollView style={s.container} showsVerticalScrollIndicator={false}>
                        <MainTitle>{i18n.t('home.me.Notifications.todo')} (2)</MainTitle>
                        <ScrollView style={s.cardsList} horizontal={true} showsHorizontalScrollIndicator={false}>
                            <AddCard
                                title={i18n.t('home.me.Notifications.addEmailTitle')}
                                description={i18n.t('home.me.Notifications.addEmailDescription')}
                            />
                            <AddCard
                                title={i18n.t('home.me.Notifications.addStreetTitle')}
                                description={i18n.t('home.me.Notifications.addStreetDescription')}
                            />
                        </ScrollView>
                        <MainTitle>{i18n.t('home.me.Notifications.messages')} ({messages.length})</MainTitle>
                        {/*<View style={s.messages}>*/}
                            {messages.map((item, index) => (
                                <MessageItem key={index} date={item.date}>{item.message}</MessageItem>
                            ))}
                        {/*</View>*/}
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    container: {
        marginHorizontal: sizes.horizontalOffset,
    },
    cardsList: {
        marginTop: 25,
        overflow: 'visible',
        flexDirection: 'row',
        paddingBottom: 35,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    }
});

const messages = [
    {
        message: 'Using Laser Treatment To Help You',
        date: '12:13 PM'
    },
    {
        message: 'Help Finding Information Online',
        date: '12:08 PM'
    },
    {
        message: 'Using Laser Treatment To Help You',
        date: '12:13 PM'
    },
    {
        message: 'Help Finding Information Online',
        date: '12:08 PM'
    },
    {
        message: 'Using Laser Treatment To Help You',
        date: '12:13 PM'
    },
    {
        message: 'Help Finding Information Online',
        date: '12:08 PM'
    },
    {
        message: 'Help Finding Information Online',
        date: '12:08 PM'
    },
    {
        message: 'Using Laser Treatment To Help You',
        date: '12:13 PM'
    },
    {
        message: 'Help Finding Information Online',
        date: '12:08 PM'
    },
];