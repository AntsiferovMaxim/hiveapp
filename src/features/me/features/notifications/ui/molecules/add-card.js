import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {ShadowContainer} from '@/ui';
import i18n from '@/utils/translate';

export const AddCard = ({title, description, onPress}) => (
    <ShadowContainer style={s.container}>
        <Text style={s.title}>{title}</Text>
        <Text style={s.description}>{description}</Text>
        <TouchableOpacity style={s.button} onPress={onPress}>
            <Text style={s.buttonText}>{i18n.t('home.me.Notifications.addNow')}</Text>
        </TouchableOpacity>
    </ShadowContainer>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        width: 250,
        marginRight: 16,
        marginVertical: 1,
        marginLeft: 1,
        elevation: 1
    },
    title: {
        fontWeight: '900',
        fontSize: 15,
        color: '#323B45',
        letterSpacing: .24
    },
    description: {
        marginTop: 9,
        fontWeight: '500',
        fontSize: 12,
        color: '#495285',
        letterSpacing: -0.18
    },
    button: {
        marginTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 140,
        height: 37,
        borderRadius: 18.5,
        backgroundColor: '#FFB300'
    },
    buttonText: {
        fontWeight: '600',
        fontSize: 14,
        color: '#fff'
    }
});
