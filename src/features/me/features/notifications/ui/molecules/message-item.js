import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';
import {colors} from '@/ui/theme';
import messageIcon from '@/assets/images/message-icon.png';

export const MessageItem = ({children, date}) => (
    <View style={s.container}>
        <Image style={s.messageIcon} source={messageIcon}/>
        <View style={s.details}>
            <Text style={s.message}>{children}</Text>
            <Text style={s.date}>{date}</Text>
        </View>
        {/*<Image/>*/}
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 11,
        paddingBottom: 11,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    },
    message: {
        fontWeight: '500',
        fontSize: 15,
        color: '#504A8E'
    },
    date: {
        marginTop: 4,
        fontSize: 12,
        color: '#8A92B9'
    },
    messageIcon: {
        width: 24,
        height: 24,
        marginRight: 16
    },
    details: {
        flex: 1
    },
    arrowIcon: {}
});
