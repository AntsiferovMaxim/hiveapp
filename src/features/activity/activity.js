import React from 'react';
import {StatusBar, View, ScrollView, StyleSheet, Text} from 'react-native';
import {styles} from "@/ui/theme";
import {Header} from '@/ui';
import {Tabs, ActivityItem, PurpleButton} from './ui';
import {logs, history} from './temp-data';

export class Activity extends React.Component {
    state = {
        activeTab: 0,
    };

    handleChangeTab = (tab) => this.setState(() => ({activeTab: tab}));

    render() {
        const {activeTab} = this.state;

        return (
            <View style={[styles.container]}>
                <StatusBar
                    barStyle="dark-content"
                />
                <Header>Activity</Header>
                <Tabs tabs={['HISTORY', 'LOGS']} onChange={this.handleChangeTab}/>
                <ScrollView style={s.scroll}>
                    {activeTab === 0 ? (
                        history.map((item, index) => (
                            <ActivityItem date={item.date} icon={item.icon} title={item.type} key={Math.random() + ''} index={index}>
                                {item.hid ? <Text style={s.hid}>{`+${item.hid} HID`}</Text> : <PurpleButton>Log out</PurpleButton>}
                            </ActivityItem>
                        ))
                    ) : (
                        logs.map((item, index) => (
                            <ActivityItem date={item.date} icon={item.icon} title={item.type} key={Math.random() + ''} index={index}/>
                        ))
                    )}
                </ScrollView>
            </View>
        )
    }
}

const s = StyleSheet.create({
    scroll: {
        marginHorizontal: 16,
        marginTop: 32,
    },
    hid: {
        fontWeight: '500',
        fontSize: 14,
        color: '#6743DF'
    }
});