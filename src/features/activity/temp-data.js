import icon from "@/features/settings/assets/settings-notify-icon.png";
import likeIcon from './assets/like-icon.png';
import starIcon from './assets/star-icon.png';

export const logs = [
    {
        type: 'Email verified',
        date: '10 hours ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Email verified',
        date: '10 hours ago',
        icon: icon
    },
    {
        type: 'Email verified',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '10 hours ago',
        icon: icon
    },
    {
        type: 'Email verified',
        date: '10 hours ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Email verified',
        date: '10 hours ago',
        icon: icon
    },
    {
        type: 'Email verified',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '11 hours 15 minutes ago',
        icon: icon
    },
    {
        type: 'Verification email sent',
        date: '10 hours ago',
        icon: icon
    },
];

export const history = [
    {
        type: 'Signed up with Airbnb',
        date: '10 hours ago',
        icon: starIcon,
        hid: 0.002
    },
    {
        type: 'Logged in to Airbnb',
        date: '11 hours 15 minutes ago',
        icon: likeIcon
    },
    {
        type: 'Signed up with Airbnb',
        date: '10 hours ago',
        icon: starIcon,
        hid: 0.002
    },
    {
        type: 'Logged in to Airbnb',
        date: '11 hours 15 minutes ago',
        icon: likeIcon
    },
    {
        type: 'Signed up with Airbnb',
        date: '10 hours ago',
        icon: starIcon,
        hid: 0.002
    },
    {
        type: 'Logged in to Airbnb',
        date: '11 hours 15 minutes ago',
        icon: likeIcon
    },
    {
        type: 'Signed up with Airbnb',
        date: '10 hours ago',
        icon: starIcon,
        hid: 0.002
    },
    {
        type: 'Logged in to Airbnb',
        date: '11 hours 15 minutes ago',
        icon: likeIcon
    },{
        type: 'Signed up with Airbnb',
        date: '10 hours ago',
        icon: starIcon,
        hid: 0.002
    },
    {
        type: 'Logged in to Airbnb',
        date: '11 hours 15 minutes ago',
        icon: likeIcon
    },
];