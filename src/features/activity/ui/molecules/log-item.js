import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {colors} from '@/ui/theme';

export const LogItem = ({icon, date, children}) => (
    <View style={s.container}>
        <Image style={s.icon} source={icon}/>
        <View style={s.details}>
            <Text style={s.title}>{children}</Text>
            <Text style={s.date}>{date}</Text>
        </View>
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 16
    },
    details: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    title: {
        fontWeight: '500',
        fontSize: 14,
        color: '#323B45'
    },
    date: {
        marginTop: 3,
        fontSize: 12,
        color: '#8A92B9'
    }
});