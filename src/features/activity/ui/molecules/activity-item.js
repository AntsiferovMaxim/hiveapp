import React from 'react';
import {View, Text, Image, StyleSheet, Animated} from 'react-native';
import {colors} from '@/ui/theme';

const delay = 30;

export class ActivityItem extends React.PureComponent {
    constructor(props) {
        super(props);

        this.animatedOpacity = new Animated.Value(0)
    }

    componentDidMount() {
        setTimeout(() => {
            Animated.timing(this.animatedOpacity, {
                toValue: 1,
                duration: 400
            }).start();
        }, this.props.index * delay)
    };

    render() {
        const {icon, date, children, title} = this.props;

        const opacity = {
            opacity: this.animatedOpacity.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1]
            })
        };

        return (
            <Animated.View style={[s.container, opacity]}>
                <Image style={s.icon} source={icon}/>
                <View style={s.details}>
                    <Text style={s.title}>{title}</Text>
                    <Text style={s.date}>{date}</Text>
                </View>
                <View style={s.actions}>
                    {children}
                </View>
            </Animated.View>
        )
    }
}

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 16
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        minWidth: 90,
        paddingRight: 4
    },
    details: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    title: {
        fontWeight: '500',
        fontSize: 14,
        color: '#323B45'
    },
    date: {
        marginTop: 3,
        fontSize: 12,
        color: '#8A92B9'
    }
});