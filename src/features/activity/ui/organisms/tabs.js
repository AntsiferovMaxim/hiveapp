import React from 'react';
import {Animated, Text, TouchableWithoutFeedback, View, StyleSheet, Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

const tabsHeight = 40;
const tabsContainerWidth = (width - 16 * 2);

export class Tabs extends React.Component {
    constructor(props) {
        super(props);

        this.animatedLeft = new Animated.Value(0);

        this.state = {
            tabWidth: tabsContainerWidth / props.tabs.length,
            active: 0,
            prev: 0,
        };
    }

    handleTabClick = (index) => {
        this.setState(({active}) => ({
            active: index,
            prev: active
        }), () => {
            this.animatedLeft.setValue(0);
            this.props.onChange(index);
            Animated.spring(this.animatedLeft, {
                toValue: 1,
            }).start();
        });
    };

    render() {
        const tabSize = StyleSheet.create({
            width: this.state.tabWidth,
            height: tabsHeight
        });

        const {active, prev} = this.state;
        const activeColor = '#ffffff';
        const inactiveColor = '#FFB300';

        const left = {
            left: this.animatedLeft.interpolate({
                inputRange: [0, 1],
                outputRange: [prev * this.state.tabWidth, active * this.state.tabWidth]
            })
        };

        const animatedActiveColor = {color: this.animatedLeft.interpolate({
            inputRange: [0, 1],
            outputRange: [inactiveColor, activeColor]
        })};

        const animatedInactiveColor =  {color: this.animatedLeft.interpolate({
            inputRange: [0, 1],
            outputRange: [activeColor, inactiveColor]
        })};

        const indicatorWidth = {
            width: this.state.tabWidth
        };

        return (
            <View style={s.tabsContainer}>
                <View style={s.tabsBackground}>
                    <Animated.View style={[s.indicator, indicatorWidth, left]}/>
                    <View style={s.absoluteContainer}>
                        {this.props.tabs.map((item, index) => (
                            <TouchableWithoutFeedback onPress={() => this.handleTabClick(index)} key={item}>
                                <View style={[s.tabsItem, tabSize]}>
                                    <Animated.Text style={[
                                        s.tabsText,
                                        active === index && s.textActive,
                                        active === index ? animatedActiveColor : animatedInactiveColor,
                                    ]}>{item}</Animated.Text>
                                </View>
                            </TouchableWithoutFeedback>
                        ))}
                    </View>
                </View>
            </View>
        )
    }
}

const s = StyleSheet.create({
    tabsContainer: {
        height: tabsHeight,
        marginHorizontal: 16,
        marginTop: 24
    },
    absoluteContainer: {
        position: 'absolute',
        width: tabsContainerWidth,
        height: tabsHeight,
        flexDirection: 'row',
        zIndex: 2
    },
    indicator: {
        position: 'absolute',
        top: 0,
        backgroundColor: 'rgba(255, 179, 0, 1)',
        borderRadius: tabsHeight / 2,
        height: tabsHeight,
        zIndex: 1
    },
    tabsBackground: {
        position: 'relative',
        height: tabsHeight,
        backgroundColor: 'rgba(255, 179, 0, .1)',
        borderRadius: tabsHeight / 2
    },
    tabsItem: {
        height: tabsHeight,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: tabsHeight / 2
    },
    active: {
        backgroundColor: 'rgb(255, 179, 0)'
    },
    tabsText: {
        fontSize: 14,
        color: '#FFB300',
        fontWeight: 'bold',
        letterSpacing: -0.34
    },
    textActive: {
        color: '#fff',
    }
});
