import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

export const PurpleButton = ({children}) => (
    <TouchableOpacity style={s.button}>
        <Text style={s.text}>{children}</Text>
    </TouchableOpacity>
);

const s = StyleSheet.create({
    button: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: 80,
        height: 24,
        borderColor: '#6743DF',
        borderWidth: 1,
        borderRadius: 12
    },
    text: {
        fontWeight: '500',
        fontSize: 14,
        color: '#6743DF'
    }
});
