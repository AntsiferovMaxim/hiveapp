import React from 'react';
import {Text, View, SafeAreaView, StyleSheet, StatusBar, Dimensions, TouchableOpacity, Image} from 'react-native';
import {withNavigationFocus} from "react-navigation";
import {RNCamera} from 'react-native-camera';

import exitIcon from '@/assets/images/exit-icon.png';
import {SubHeader, YellowButton} from '@/ui';
import {fontSize, colors} from "@/ui/theme";

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');

class ScanScreen extends React.Component {
    state = {
        permissions: false
    };

    onBarcodeDetected = ({barcodes}) => {
        console.log(barcodes)
    };

    handleExit = () => {
      this.props.navigation.navigate('Me')
    };

    renderCamera = () => {
        const isFocused = this.props.navigation.isFocused();

        if (!isFocused) {
            return null;
        }

        return (
            <View style={{flex: 1}}>
                <StatusBar hidden={true}/>
                <RNCamera
                    style={s.camera}
                    ref={ref => {
                        this.camera = ref;
                    }}
                    type={RNCamera.Constants.Type.back}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    onGoogleVisionBarcodesDetected={this.onBarcodeDetected}
                >
                    <View style={s.bordered}/>
                    <TouchableOpacity style={s.exitButton} onPress={this.handleExit}>
                        <Image style={s.exitButtonIcon} source={exitIcon}/>
                    </TouchableOpacity>
                </RNCamera>
            </View>
        );
    };

    render() {
        if (this.state.permissions) {
            return this.renderCamera()
        }

        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <SubHeader onBack={this.props.navigation.goBack}>Scan</SubHeader>
                    <View style={s.container}>
                        <Text style={s.title}>Permission to use camera</Text>
                        <Text style={s.description}>We need your permission to use your camera phone</Text>
                        <YellowButton style={s.button} onPress={() => this.setState({permissions: true})}>Scan</YellowButton>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const size = SCREEN_WIDTH * .8;
const marginTop = SCREEN_HEIGHT / 2 - size / 2;
const marginLeft = SCREEN_WIDTH / 2 - size / 2;

const s = StyleSheet.create({
    camera: {
        flex: 1,
        position: 'relative'
    },
    bordered: {
        position: 'absolute',
        marginTop: marginTop,
        marginLeft: marginLeft,
        width: size,
        height: size,
        borderWidth: 2,
        borderColor: colors.dividedBorder,
        overflow: 'visible',
    },
    text: {
        color: 'white'
    },
    exitButtonIcon: {
        width: 28,
        height: 28
    },
    exitButton: {
        position: 'relative',
        top: marginTop + size + 50,
        left: SCREEN_WIDTH / 2 - 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        marginTop: 23,
        fontSize: fontSize(16),
        fontWeight: '600',
        color: '#323B45',
        opacity: .8
    },
    description: {
        marginTop: 13,
        fontSize: fontSize(14),
        color: '#767FAB',
        opacity: .8,
        textAlign: 'center',
        maxWidth: 320
    },
    button: {
        marginTop: 30,
        paddingHorizontal: 25
    }
});

export const ScanQr = withNavigationFocus(ScanScreen);
