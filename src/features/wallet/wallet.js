import React from 'react';
import {View, StyleSheet, StatusBar, ScrollView, Text} from 'react-native';
import {styles, colors} from "@/ui/theme";
import {Header, MainTitleWithAddButton, MainTitle, ShadowContainer} from '@/ui';
import {MockCards, CreditCard, BalanceCard} from './ui';

export class Wallet extends React.Component {
    state = {
        cards: []
    };

    handleAddCard = () => {
      this.setState(({cards}) => ({
          cards: [...cards, {
              number: '6573 **** **** 8423',
              expDate: '11/19'
          }]
      }));
    };

    render() {
        const isEmpty = this.state.cards.length === 0;

        return (
            <View style={styles.container}>
                <StatusBar
                    barStyle="dark-content"
                />
                <Header>Wallet</Header>
                <View style={s.container}>
                    <MainTitleWithAddButton onAdd={this.handleAddCard}>Virtual Cards</MainTitleWithAddButton>
                    <ScrollView
                        style={s.cardsContainer}
                        justifyContent={isEmpty ? 'center' : 'flex-start'}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    >
                        {isEmpty ? <MockCards/> : this.state.cards.map((item, index) => (
                            <CreditCard expDate={item.expDate} key={item.number + '' + index}>
                                {item.number}
                            </CreditCard>
                        ))}
                    </ScrollView>
                    <MainTitle>HID Balance</MainTitle>
                    <BalanceCard>0.82738</BalanceCard>
                </View>
            </View>
        )
    }
}

const s = StyleSheet.create({
    container: {
        paddingHorizontal: 10,
        flexDirection: 'column'
    },
    cardsContainer: {
        flexDirection: 'row',
        minHeight: 190,
        marginTop: 10,
        borderBottomColor: colors.dividedBorder,
        borderBottomWidth: 1
    },
    emptyCards: {
        justifyContent: 'center'
    }
});