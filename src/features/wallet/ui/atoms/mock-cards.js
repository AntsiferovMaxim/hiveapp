import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

import noCardsImage from '../../assets/no-credit-cards.png';

export const MockCards = () => (
    <View style={s.container}>
        <Image style={s.image} source={noCardsImage}/>
        <Text style={s.text}>Add data Profile</Text>
        <Text style={s.text}>bottom to build your profile</Text>
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    image: {
        width: 64,
        height: 52,
        marginBottom: 13
    },
    text: {
        fontSize: 14,
        color: '#9BA1A7'
    }
});
