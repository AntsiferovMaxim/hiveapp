import React from 'react';
import {View, Image, StyleSheet, Text} from 'react-native';

import backgroundCard from '../../assets/credit-card-background.png';
import hiveLogo from '../../assets/logo-white.png';
import visaLogo from '../../assets/visa-icon.png';
import moreIcon from '@/assets/images/more-icon-white.png';

const coef = 1.688;
const cardWidth = 287;
const cardHeight = cardWidth / coef;

export const CreditCard = ({children, expDate}) => (
    <View style={s.container}>
        <Image style={s.backgroundImage} source={backgroundCard}/>
        <View style={s.absoluteContainer}>
            <View style={s.row}>
                <Image style={s.logo} source={hiveLogo}/>
                <Image style={s.moreIcon} source={moreIcon}/>
            </View>
            <View style={s.middleRow}>
                <Text style={s.cardNumberTitle}>CARD NUMBER</Text>
                <Text style={s.number}>{children}</Text>
            </View>
            <View style={s.row}>
                <View style={s.expContainer}>
                    <Text style={s.expText}>EXP.DATE</Text>
                    <Text style={s.expText}>{expDate}</Text>
                </View>
                <Image style={s.visaLogo} source={visaLogo}/>
            </View>
        </View>
    </View>
);

const s = StyleSheet.create({
    container: {
        position: 'relative',
        marginRight: 16
    },
    backgroundImage: {
        width: cardWidth,
        height: cardHeight
    },
    absoluteContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        flexDirection: 'column',
        justifyContent: 'space-between',
        width: cardWidth,
        height: cardHeight,
        padding: 15,
        zIndex: 1
    },
    expContainer: {
        flexDirection: 'column',
    },
    expText: {
        color: 'white',
        fontSize: 12
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    cardNumberTitle: {
        fontSize: 12,
        color: '#fff',
        textTransform: 'uppercase'
    },
    number: {
        fontWeight: '900',
        fontSize: 20,
        color: '#fff',
        letterSpacing: 1.11,
        textShadowColor: 'rgba(0,0,0,0.28)',
        textShadowOffset: {width: 0, height: 1},
        textShadowRadius: 2
    },
    middleRow: {
        flexDirection: 'column',
        alignItems: 'flex-start'
    },
    logo: {
        width: 76,
        height: 29
    },
    moreIcon: {
        width: 24,
        height: 24
    },
    visaLogo: {
        width: 44,
        height: 14
    }
});
