import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

import {ShadowContainer, YellowButton} from '@/ui';
import moreIcon from '@/assets/images/more-icon.png';

export const BalanceCard = ({children}) => (
    <ShadowContainer style={s.card}>
        <View style={s.cardHeader}>
            <Text style={s.title}>BALANCE</Text>
            <Image style={s.moreIcon} source={moreIcon}/>
        </View>
        <Text style={s.balance}>{children}</Text>
        <View style={s.buttonsContainer}>
            <YellowButton onPress={() => console.log('RECEIVE')} style={[s.btn, s.first]}>
                RECEIVE
            </YellowButton>
            <YellowButton onPress={() => console.log('SEND')} style={[s.btn, s.last]}>
                SEND
            </YellowButton>
        </View>
    </ShadowContainer>
);

const s = StyleSheet.create({
    card: {
        borderRadius: 10
    },
    title: {
        fontSize: 12,
        fontWeight: '500',
        letterSpacing: -.18,
        color: '#495285',
        textTransform: 'uppercase'
    },
    balance: {
        marginTop: 10,
        fontSize: 20,
        fontWeight: '900',
        color: '#495285',
        letterSpacing: .32
    },
    buttonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 24
    },
    cardHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    btn: {
        flex: 1
    },
    first: {
        marginRight: 15
    },
    last: {
        marginLeft: 15
    },
    moreIcon: {
        width: 24,
        height: 24
    }
});
