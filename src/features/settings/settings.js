import React from 'react';
import {View, StyleSheet, ScrollView, SafeAreaView} from 'react-native';
import {Header, StatusBar} from '@/ui';
import {SettingsItem} from './ui';

import icon from './assets/settings-notify-icon.png';

export class Settings extends React.Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <View style={{flex: 1}}>
                    <StatusBar/>
                    <Header>Settings</Header>
                    <ScrollView style={s.settingsList}>
                        {settingsItems.map((item, index) => (
                            item.title ?
                                <SettingsItem
                                    icon={item.icon}
                                    preview={item.preview}
                                    key={item.title}
                                    onPress={() => this.props.navigation.navigate(item.to)}
                                >
                                    {item.title}
                                </SettingsItem> :
                                <SettingsItem.Empty key={index}/>
                        ))}
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const settingsItems = [
    {
        title: 'Notifications',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'Privacy and Security',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'Data and Storage',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'Language',
        icon: icon,
        preview: 'English',
        to: 'SettingsLanguage'
    },
    {
        title: 'Alternative Currency',
        icon: icon,
        preview: 'US Dollar',
        to: 'SettingsNotifications'
    },
    {},
    {
        title: 'Help  & Support',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'Share Hive',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'Give us feedback',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
    {
        title: 'About Hive',
        icon: icon,
        preview: undefined,
        to: 'SettingsNotifications'
    },
];

const s = StyleSheet.create({
    settingsList: {
        paddingHorizontal: 10,
        paddingTop: 15
    }
});
