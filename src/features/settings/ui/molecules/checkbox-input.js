import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Checkbox} from '@/ui';
import {colors, fontSize} from "@/ui/theme";

export const CheckboxInput = ({children, onChange}) => (
    <View style={s.container}>
        <Text style={s.text}>{children}</Text>
        <Checkbox onChange={onChange}/>
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    },
    text: {
        fontSize: fontSize(14),
        color: '#323B45'
    }
});
