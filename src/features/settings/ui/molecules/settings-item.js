import React from 'react';
import {View, StyleSheet, Image, Text, TouchableWithoutFeedback} from 'react-native';

import rightArrow from '../../assets/right-arrow.png';

const SettingsItem = ({icon, children, preview, onPress}) => (
    <TouchableWithoutFeedback onPress={onPress}>
        <View style={s.container}>
            <View style={s.left}>
                <Image style={s.icon} source={icon}/>
                <Text style={s.settingsTitle}>{children}</Text>
            </View>
            <View style={s.right}>
                {preview && <Text style={s.preview}>{preview}</Text>}
                <Image style={s.arrowIcon} source={rightArrow}/>
            </View>
        </View>
    </TouchableWithoutFeedback>
);

SettingsItem.Empty = () => (
    <View style={[s.container, s.empty]}/>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        marginHorizontal: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#E9ECF9'
    },
    empty: {
        minHeight: 49
    },
    left: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    right: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 16
    },
    arrowIcon: {
        width: 5,
        height: 8
    },
    preview: {
        fontSize: 14,
        color: '#6743DF',
        fontWeight: '500',
        marginRight: 11
    },
    settingsTitle: {
        fontSize: 14,
        color: '#495285',
        fontWeight: '500'
    }
});

export {SettingsItem};