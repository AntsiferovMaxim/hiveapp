import { createStackNavigator } from 'react-navigation';

import {Settings} from './settings';
import {SettingsNotifications} from './features/settings-notifications';
import {SettingsLanguage} from './features/settings-language';

export default createStackNavigator({
    Settings: {
        screen: Settings
    },
    SettingsNotifications: {
        screen: SettingsNotifications
    },
    SettingsLanguage: {
        screen: SettingsLanguage
    }
}, {
    initialRouteName: 'Settings',
    headerMode: 'none',
    cardStyle: { backgroundColor: '#FFFFFF' },
});
