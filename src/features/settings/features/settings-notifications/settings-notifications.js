import React from 'react';
import {View, SafeAreaView, StyleSheet, Text, ScrollView} from 'react-native';

import {Header, HeaderBackButton, StatusBar} from '@/ui';
import {fontSize, sizes, colors} from '@/ui/theme';
import {CheckboxInput} from '../../ui';

export class SettingsNotifications extends React.Component {
    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <StatusBar/>
                <View style={{flex: 1}}>
                    <Header
                        LeftContainer={<HeaderBackButton onPress={this.props.navigation.goBack}>Settings</HeaderBackButton>}
                    >Notifications</Header>
                    <ScrollView style={s.container} showsVerticalScrollIndicator={false}>
                        <Text style={s.title}>Email</Text>
                        <Text style={s.description}>
                            General promotions, updates, news about Amberity or
                            general promotions for partner campaigns and services,
                            user surveys, inspiration.
                        </Text>
                        <CheckboxInput>General messages</CheckboxInput>
                        <CheckboxInput>Promotions and tips</CheckboxInput>
                        <CheckboxInput>Account activity</CheckboxInput>
                        <Text style={s.title}>Something</Text>
                        <Text style={s.description}>
                            General promotions, updates, news about Amberity or
                            general promotions for partner campaigns and services,
                            user surveys, inspiration.
                        </Text>
                        <CheckboxInput>General messages</CheckboxInput>
                        <CheckboxInput>Promotions and tips</CheckboxInput>
                        <CheckboxInput>Account activity</CheckboxInput>
                        <Text style={s.title}>Email</Text>
                        <Text style={s.description}>
                            General promotions, updates, news about Amberity or
                            general promotions for partner campaigns and services,
                            user surveys, inspiration.
                        </Text>
                        <CheckboxInput>General messages</CheckboxInput>
                        <CheckboxInput>Promotions and tips</CheckboxInput>
                        <CheckboxInput>Account activity</CheckboxInput>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
const s = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: sizes.horizontalOffset + 5
    },
    title: {
        marginTop: 20,
        fontWeight: '900',
        fontSize: fontSize(22),
        color: '#323B45'
    },
    description: {
        marginTop: 8,
        fontSize: fontSize(12),
        color: '#90959A',
        paddingBottom: 16,
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    }
});
