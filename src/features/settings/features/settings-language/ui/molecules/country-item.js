import {StyleSheet, Text, View, TouchableWithoutFeedback, Image} from "react-native";
import React from "react";
import {fontSize} from "@/ui/theme";

import purpleCheck from '@/assets/images/purple-check.png';

export const CountyItem = ({selected, children, id, onPress}) => (
    <TouchableWithoutFeedback onPress={() => onPress(id)}>
        <View style={s.container}>
            <Text style={[s.country, selected && s.selected]}>{children}</Text>
            {selected && <Image style={s.icon} source={purpleCheck}/>}
        </View>
    </TouchableWithoutFeedback>
);

const s = StyleSheet.create({
    country: {
        fontSize: fontSize(16),
        color: '#495285'
    },
    container: {
        paddingVertical: 13,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    selected: {
        color: '#6743DF',
        fontWeight: '600'
    },
    icon: {
        width: 10,
        height: 9
    }
});
