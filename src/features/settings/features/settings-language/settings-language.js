import React from 'react';
import {View, SafeAreaView, StyleSheet, ScrollView} from 'react-native';

import {Header, HeaderBackButton, Search} from '@/ui';
import {sizes} from "@/ui/theme";
import {CountyItem} from './ui';
import countries from './countries';

export class SettingsLanguage extends React.Component {
    state = {
        language: countries[0].id,
        searchQuery: ''
    };

    filteredCountries() {
        const {searchQuery} = this.state;

        return countries.filter(item => {
            return item.country.toLowerCase().indexOf(searchQuery.toLowerCase()) >= 0
        })
    }

    handleSearchChange = (text) => this.setState(() => ({
        searchQuery: text
    }));

    handleSelectCountry = (id) => this.setState(() => ({
        language: id
    }));

    render() {
        const goBack = this.props.navigation.goBack;
        const filteredCountries = this.filteredCountries();

        return (
            <SafeAreaView style={{flex: 1}}>
                <Header
                    LeftContainer={<HeaderBackButton onPress={goBack}>Settings</HeaderBackButton>}
                    BottomContainer={<Search onChange={this.handleSearchChange}/>}
                >Language</Header>
                <View style={s.container}>
                    <ScrollView>
                        {filteredCountries.map(({country, id}) => (
                            <CountyItem
                                key={id}
                                id={id}
                                selected={this.state.language === id}
                                onPress={this.handleSelectCountry}
                            >
                                {country}
                            </CountyItem>
                        ))}
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}

const s = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: sizes.horizontalOffset + 5,
        marginTop: 30
    }
});


