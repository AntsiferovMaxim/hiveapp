import React from 'react';
import {createBottomTabNavigator} from "react-navigation";

import {colors} from '@/ui/theme';
import {MeRoute} from './features/me';
import {SettingsRoute} from './features/settings';
import {ScanQr} from './features/scan-qr';
import {MeIcon, SettingsIcon, ScanIcon, NavIcon} from './ui';

export const RootStack = createBottomTabNavigator({
    MeRoute: {
        screen: MeRoute,
        navigationOptions: (options) => ({
            tabBarIcon: NavIcon('Me', MeIcon),
            tabBarVisible: getRouteName(options.navigation.state) === 'Me',
        })
    },
    ScanQr: {
      screen: ScanQr,
        navigationOptions: () => ({
            tabBarIcon: ScanIcon,
            tabBarVisible: false,
        })
    },
    Settings: {
        screen: SettingsRoute,
        navigationOptions: () => ({
            tabBarIcon: NavIcon('Settings', SettingsIcon)
        })
    }
}, {
    initialRouteName: 'MeRoute',
    tabBarOptions: {
        showLabel: false,
        activeTintColor: colors.nav.active,
        inactiveTintColor: colors.nav.inactive,
    },
});

function getRouteName(state) {
    return state.routes[state.index].routeName
}

