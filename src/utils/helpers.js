export function range(to) {
    return [...Array(to).keys()]
}