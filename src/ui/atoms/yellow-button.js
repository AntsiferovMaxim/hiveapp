import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';

export const YellowButton = ({onPress, children, style}) => (
    <TouchableOpacity style={[s.btn, style]} onPress={onPress}>
        <Text style={s.text}>{children}</Text>
    </TouchableOpacity>
);

const s = StyleSheet.create({
    btn: {
        height: 38,
        borderRadius: 19,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFB300'
    },
    text: {
        fontWeight: '600',
        fontSize: 14,
        color: 'white',
    }
});
