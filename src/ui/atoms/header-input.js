import React from 'react';
import {View, TextInput, StyleSheet, Platform, Image, TouchableWithoutFeedback} from 'react-native';
import {fontSize} from "@/ui/theme";

import cancelIcon from '@/assets/images/cancel-icon.png';

export const HeaderInput = ({placeholder, children, style, onClear, onChange, value}) => (
    <View style={s.container}>
        <TextInput
            style={[s.input, style]}
            placeholder={placeholder}
            placeholderTextColor="#A2A7AC"
            onChangeText={onChange}
            value={value}
        />
        {onClear && (
            <TouchableWithoutFeedback onPress={onClear}>
                <Image style={s.icon} source={cancelIcon}/>
            </TouchableWithoutFeedback>
        )}
        {children}
    </View>
);

const inputHeight = 36;
const iconSize = 16;

const s = StyleSheet.create({
    container: {
        position: 'relative'
    },
    icon: {
        position: 'absolute',
        right: 10,
        top: inputHeight / 2 - iconSize / 2,
        width: iconSize,
        height: iconSize
    },
    input: {
        paddingVertical: Platform.OS === 'ios' ? 7.7 : 4,
        paddingLeft: 10,
        borderRadius: 10,
        fontSize: fontSize(17),
        color: '#323B45',
        letterSpacing: -.4,
        backgroundColor: 'rgba(143, 142, 147, 0.1)',
    }
});
