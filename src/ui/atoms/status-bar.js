import {StatusBar as RNStatusBar} from "react-native";
import React from "react";

export const StatusBar = () => (
    <RNStatusBar
        backgroundColor="white"
        barStyle="dark-content"
    />
);