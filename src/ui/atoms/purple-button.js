import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';

export const PurpleButton = ({children, style, onPress}) => (
    <TouchableOpacity style={[s.container, style]} onPress={onPress}>
        <Text style={s.text}>{children}</Text>
    </TouchableOpacity>
);

const s = StyleSheet.create({
    container: {
        height: 40,
        borderRadius: 20,
        backgroundColor: '#6743DF',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 24
    },
    text: {
        fontSize: 18,
        color: '#fff'
    }
});
