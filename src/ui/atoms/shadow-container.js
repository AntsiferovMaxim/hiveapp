import React from 'react';
import {View, StyleSheet} from 'react-native';

export const ShadowContainer = ({children, style}) => (
    <View style={[s.container, style]}>{children}</View>
);

const s = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        padding: 16,
        borderRadius: 6,
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 10,
    }
});
