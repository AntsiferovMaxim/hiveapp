import React from 'react';
import {SafeAreaView, ScrollView, View} from 'react-native';

import {Header, HeaderBackButton, StatusBar} from '../';

export const Screen = ({title, onBack, from, fromTitle, children}) => (
    <SafeAreaView style={{flex: 1}}>
        <View style={{flex: 1}}>
            <StatusBar/>
            <Header
                LeftContainer={
                    <HeaderBackButton onPress={onBack} from={from}>
                        {fromTitle}
                    </HeaderBackButton>
                }
            >
                {title}
            </Header>
            <ScrollView>
                {children}
            </ScrollView>
        </View>
    </SafeAreaView>
);