export const colors = {
    nav: {
        active: '#6743DF',
        inactive: '#576090'
    },
    dividedBorder: '#E9ECF9'
};
