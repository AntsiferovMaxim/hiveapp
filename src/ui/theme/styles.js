import {StyleSheet} from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: isIphoneX() ? 35 : 20,
    }
});