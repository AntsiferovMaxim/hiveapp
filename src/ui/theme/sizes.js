export function fontSize(size) {
    const coef = 1;

    return size * coef;
}

export const sizes = {
    headerTitle: fontSize(30),
    headerBackText: fontSize(17),
    navCardTitle: fontSize(17),
    navCardDescription: fontSize(12),
    sectionTitle: fontSize(22),
    sectionDescription: fontSize(12),

    horizontalOffset: 20
};