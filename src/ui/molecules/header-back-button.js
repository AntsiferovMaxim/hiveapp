import React from "react";
import {Image, StyleSheet, Text, TouchableWithoutFeedback, View} from "react-native";
import leftArrow from '@/assets/images/left-arrow.png';
import {sizes} from '@/ui/theme';

export const HeaderBackButton = ({children, onPress, from}) => (
    <TouchableWithoutFeedback onPress={() => onPress(from)}>
        <View style={s.container}>
            <Image source={leftArrow} style={s.image}/>
            <Text style={s.backButton}>{children}</Text>
        </View>
    </TouchableWithoutFeedback>
);

const s = StyleSheet.create({
    backButton: {
        fontSize: sizes.headerBackText,
        color: '#576090'
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        width: 8,
        height: 16,
        marginRight: 10
    }
});