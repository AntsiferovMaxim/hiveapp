import React from 'react';
import {TouchableWithoutFeedback, Image, StyleSheet} from 'react-native';

import addButton from '../../assets/images/add-button.png';

export const HeaderAddButton = ({onPress}) => (
    <TouchableWithoutFeedback onPress={onPress}>
        <Image style={s.image} source={addButton}/>
    </TouchableWithoutFeedback>
);

const s = StyleSheet.create({
    image: {
        width: 30,
        height: 30
    }
});
