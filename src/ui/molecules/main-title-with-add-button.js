import React from 'react';
import {Image, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import {MainTitle} from './main-title';

import addImage from '@/assets/images/add-button.png';
import addImagePurple from '@/assets/images/add-button-purple.png';

export const MainTitleWithAddButton = ({children, onAdd, purple}) => (
    <MainTitle
        RightContainer={<AddButton onPress={onAdd} addIcon={purple ? addImagePurple : addImage}/>}
    >{children}</MainTitle>
);

const AddButton = ({onPress, addIcon}) => (
    <TouchableWithoutFeedback onPress={onPress}>
        <Image style={s.icon} source={addIcon}/>
    </TouchableWithoutFeedback>
);

const s = StyleSheet.create({
    icon: {
        width: 29,
        height: 29
    }
});