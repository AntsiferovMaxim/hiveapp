import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";
import {sizes} from '@/ui/theme';

export const NavCard = ({title, children, color, onPress, to}) => (
    <TouchableOpacity style={styles.opacityContainer} onPress={() => onPress(to)}>
        <View style={styles.card}>
            <Text style={styles.cardTitle}>{title}</Text>
            {children}
            <View style={[styles.circle, {backgroundColor: color}]}/>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    opacityContainer: {
        borderRadius: 6,
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 1,
        marginRight: 16,
        marginVertical: 2,
        marginLeft: 2
    },
    card: {
        position: 'relative',
        width: 208,
        height: 116,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
        borderRadius: 6,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    circle: {
        position: 'absolute',
        right: -25,
        top: -25,
        height: 108,
        width: 108,
        borderRadius: 54,
    },
    cardTitle: {
        color: '#495285',
        maxWidth: 100,
        fontSize: sizes.navCardTitle,
        fontWeight: "900"
    },
    cardText: {
        color: '#495285',
        fontSize: 12
    },
});