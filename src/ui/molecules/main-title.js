import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {sizes} from "@/ui/theme/sizes";

export const MainTitle = ({children, RightContainer}) => (
    <View style={s.container}>
        <Text style={s.title}>{children}</Text>
        {RightContainer}
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 18,
        marginBottom: 8,
        marginHorizontal: 5
    },
    title: {
        fontSize: sizes.sectionTitle,
        color: '#323B45',
        fontWeight: '900',
        letterSpacing: .35
    }
});