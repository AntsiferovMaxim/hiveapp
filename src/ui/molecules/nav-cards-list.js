import React from 'react';
import {Text, View, StyleSheet, ScrollView} from 'react-native';
import {sizes} from '@/ui/theme';

const NavCardsList = ({children, title, description}) => (
    <View>
        <Text style={styles.header}>{title}</Text>
        <Text style={styles.desc}>{description}</Text>
        <ScrollView
            horizontal={true}
            style={styles.scrollView}
            showsHorizontalScrollIndicator={false}
        >
            {children}
        </ScrollView>
    </View>
);

const styles = StyleSheet.create({
    scrollView: {
        overflow: 'visible',
    },
    header: {
        fontSize: sizes.sectionTitle,
        fontWeight: "900",
        color: '#323B45'
    },
    desc: {
        marginBottom: 10,
        fontSize: sizes.sectionDescription,
        fontWeight: "500",
        color: '#A2A7AC'
    },
});

export {NavCardsList};