import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from "react-native";
import {sizes} from '@/ui/theme';

export const SmallNavCard = ({title, color, to, onPress}) => (
    <TouchableOpacity style={styles.opacityContainer} onPress={() => onPress(to)}>
        <View style={styles.card}>
            <Text style={styles.cardTitle}>{title}</Text>
            <View style={[styles.circle, {backgroundColor: color}]}/>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    opacityContainer: {
        borderRadius: 6,
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 1,
        marginRight: 16,
        marginVertical: 2,
        marginLeft: 2,
    },
    card: {
        position: 'relative',
        width: 116,
        height: 116,
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
        borderRadius: 6,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    circle: {
        position: 'absolute',
        right: -25,
        bottom: -45,
        height: 94,
        width: 94,
        borderRadius: 54,
    },
    cardTitle: {
        color: '#495285',
        maxWidth: 80,
        fontSize: sizes.navCardTitle,
        fontWeight: "900"
    },
});