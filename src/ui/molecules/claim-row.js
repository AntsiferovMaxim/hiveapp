import React from 'react';
import {View, Text, Image, StyleSheet, Animated, TouchableWithoutFeedback} from 'react-native';
import Interactable from 'react-native-interactable';

import successIcon from '@/assets/images/success-icon.png';
import warningIcon from '@/assets/images/warning-icon.png';
import deleteIcon from '@/assets/images/delete-icon.png';

import {ShadowContainer} from '../';
import {colors} from '../theme';

export class ClaimRow extends React.Component {
    constructor(props) {
        super(props);

        this.animatedWidth = new Animated.Value(0);
        this.state = {
            height: 0
        };
    }

    render() {
        const {
            children,
            isVerify,
            id,
            onDelete,
            icon,
            iconStyle
        } = this.props;
        const width = {
            width: this.animatedWidth.interpolate({
                inputRange: [-100, -3, 0],
                outputRange: [90, 0, 0]
            }),
            height: this.state.height
        };

        const opacity = {
            opacity: this.animatedWidth.interpolate({
                inputRange: [-100, 0],
                outputRange: [1, 0]
            })
        };

        const imageWidth = {
            width: this.animatedWidth.interpolate({
                inputRange: [-100, -60, -59, 0],
                outputRange: [20, 15, 0, 0]
            }),
            height: this.animatedWidth.interpolate({
                inputRange: [-100, -60, -59, 0],
                outputRange: [20, 15, 0, 0]
            })
        };

        return (
            <View style={s.relativeContainer} onLayout={(event) => {
                this.setState({
                    height: event.nativeEvent.layout.height
                })
            }}>
                <TouchableWithoutFeedback onPress={() => onDelete(id)}>
                    <Animated.View style={[s.deleteContainer, width]}>
                        <Animated.Image style={[s.deleteIcon, opacity, imageWidth]} source={deleteIcon}/>
                    </Animated.View>
                </TouchableWithoutFeedback>
                <Interactable.View
                    horizontalOnly={true}
                    snapPoints={[{x: 0}, {x: -100}]}
                    animatedValueX={this.animatedWidth}
                >
                    <ShadowContainer
                        style={s.container}
                    >
                        <View style={[s.details, s.row]}>
                            <Image style={[s.claimIcon, iconStyle]} source={icon}/>
                            <Text style={s.email}>{children}</Text>
                            <Image style={s.statusImage} source={isVerify ? successIcon : warningIcon}/>
                        </View>
                        <View style={s.row}>
                            <Text style={[s.status, isVerify ? s.verified : s.notVerified]}>
                                {isVerify ? 'Verified' : 'This email is not verified. Complete the procedure.'}
                            </Text>
                        </View>
                    </ShadowContainer>
                </Interactable.View>
            </View>
        )
    }
}

const s = StyleSheet.create({
    deleteContainer: {
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        maxWidth: 90,
        height: 87,
        right: 0,
        top: 0,
        borderRadius: 6,
        backgroundColor: '#EE6365',
        shadowColor: 'rgb(33,35,40)',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.14,
        shadowRadius: 10,
        elevation: 5,
    },
    deleteIcon: {
        maxWidth: 25,
        maxHeight: 25
    },
    relativeContainer: {
        position: 'relative',
        marginTop: 16,
    },
    container: {
        flexDirection: 'column',
        padding: 0,
        zIndex: 1000,
        position: 'relative'
    },
    row: {
        padding: 12
    },
    claimIcon: {
        marginRight: 16
    },
    details: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: colors.dividedBorder
    },
    email: {
        flex: 1,
        fontWeight: '900',
        fontSize: 15,
        color: '#323B45',
        letterSpacing: .24
    },
    statusImage: {
        width: 24,
        height: 24
    },
    status: {
        fontSize: 14,
        fontWeight: '500'
    },
    verified: {
        color: '#22CC4E'
    },
    notVerified: {
        color: '#F8AC24'
    }
});
