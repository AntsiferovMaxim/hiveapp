import React from 'react';
import {View, StyleSheet, TouchableWithoutFeedback, Animated} from 'react-native';
import Interactable from 'react-native-interactable';

const checkboxWidth = 52;
const checkboxHeight = 28;
const circleWidth = checkboxHeight;

const leftPoint = circleWidth - checkboxWidth;
const rightPoint = 0;

export class Checkbox extends React.Component {
    constructor() {
        super();

        this._deltaX = new Animated.Value(0);
    }

    update = () => {
        const checked = this._deltaX._value > -1;

        this.onChange && this.onChange(checked);
    };

    handleClick = () => {
        const index = this._deltaX._value > -1 ? 1 : 0;

        this.refs.circle.snapTo({index});
        this.update();
    };

    handleStop = () => {
        this.update();
    };

    render() {
        const backgroundColor = this._deltaX.interpolate({
            inputRange: [leftPoint, rightPoint],
            outputRange: ['#cecece', '#FFB300']
        });

        return (
            <Animated.View style={[s.container, {backgroundColor}]}>
                <TouchableWithoutFeedback onPress={this.handleClick}>
                    <Interactable.View
                        ref='circle'
                        snapPoints={[{x: rightPoint}, {x: leftPoint}]}
                        horizontalOnly={true}
                        style={[s.circleContainer]}
                        boundaries={{left: leftPoint, right: rightPoint, bounce: .1}}
                        friction={0.7}
                        animatedValueX={this._deltaX}
                        onStop={this.handleStop}
                    >

                        <View style={s.circle}/>
                    </Interactable.View>
                </TouchableWithoutFeedback>
            </Animated.View>
        )
    }
}

const s = StyleSheet.create({
    container: {
        position: 'relative',
        width: checkboxWidth,
        height: checkboxHeight,
        borderRadius: checkboxHeight / 2,
        backgroundColor: '#FFB300'
    },
    circleContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        width: circleWidth,
        height: checkboxHeight,
    },
    circle: {
        position: 'absolute',
        top: 0,
        right: 0,
        width: circleWidth,
        height: checkboxHeight,
        backgroundColor: 'white',
        borderRadius: checkboxHeight / 2,
        shadowColor: 'rgb(0,0,0)',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.05,
        shadowRadius: 1,
        elevation: 1
    }
});
