import React from 'react';
import {View, TextInput as ReactNativeInput, Text, StyleSheet} from 'react-native';

export const TextInput = ({children, ...props}) => (
    <View>
        <Text style={s.label}>{children}</Text>
        <ReactNativeInput style={s.input} {...props}/>
    </View>
);

const s = StyleSheet.create({
    label: {
        fontSize: 12,
        color: '#767FAB',
        letterSpacing: 0.3,
        paddingLeft: 1
    },
    input: {
        borderBottomWidth: 1,
        borderBottomColor: '#6743DF',
        paddingLeft: 1,
        paddingBottom: 3,
        paddingTop: 6,
        fontSize: 16,
        fontWeight: '500',
        color: '#504A8E',
        letterSpacing: 0.4
    }
});

