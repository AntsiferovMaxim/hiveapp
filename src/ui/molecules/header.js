import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {sizes, colors} from '@/ui/theme';


export const Header = ({children, LeftContainer, RightContainer, BottomContainer}) => (
    <View style={styles.container}>
        <View style={styles.leftContainer}>
            {LeftContainer}
        </View>
        <View style={styles.footer}>
            <Text style={styles.text}>{children}</Text>
            {RightContainer}
        </View>
        {BottomContainer && (
            <View style={styles.bottomContent}>
                {BottomContainer}
            </View>
        )}
    </View>
);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        paddingHorizontal: sizes.horizontalOffset,
        paddingBottom: 10,
        borderBottomColor: colors.dividedBorder,
        borderBottomWidth: 1,
    },
    leftContainer: {
        height: 36,
        justifyContent: 'center',
        paddingRight: 6,
    },
    footer: {
        paddingRight: 6,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    text: {
        fontFamily: 'Lato-Bold',
        fontSize: sizes.headerTitle,
        color: '#323B45',
    },
    bottomContent: {
        paddingVertical: 10
    }
});
