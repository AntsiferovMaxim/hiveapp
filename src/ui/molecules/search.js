import React from 'react';
import {StyleSheet, Image} from 'react-native';

import searchIcon from '@/assets/images/search-icon.png';
import {HeaderInput} from '../atoms';

export const Search = ({children, onChange}) => (
    <HeaderInput
        placeholder={children || 'Search'}
        onChange={onChange}
        style={s.input}
        onClear={() => console.log()}
    >
        <Image style={s.icon} source={searchIcon}/>
    </HeaderInput>
);

const iconSize = 14;
const iconLeftMargin = 10;
const iconRightMargin = 7;
const searchHeight = 36;

const s = StyleSheet.create({
    input: {
        paddingLeft: iconLeftMargin + iconSize + iconRightMargin
    },
    icon: {
        left: iconLeftMargin,
        position: 'absolute',
        width: iconSize,
        height: iconSize,
        top: searchHeight / 2 - iconSize / 2
    },
});
