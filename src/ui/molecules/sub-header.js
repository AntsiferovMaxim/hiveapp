import React from 'react';
import {View, StyleSheet, Text, TouchableWithoutFeedback} from 'react-native';
import {HeaderBackButton} from './header-back-button';
import {fontSize, sizes} from "@/ui/theme";

export const SubHeader = ({backText, children, onBack, onRightButton, rightText, bottomContainer, onLayout}) => (
    <View style={s.container} onLayout={onLayout}>
        <View style={s.row}>
            <View style={s.left}>
                <HeaderBackButton onPress={onBack}>{backText || 'Back'}</HeaderBackButton>
            </View>
            <Text style={[s.text, s.center]}>{children}</Text>
            <View style={s.right}>
                {rightText && (
                    <TouchableWithoutFeedback onPress={onRightButton}>
                        <Text style={s.headerButton}>{rightText}</Text>
                    </TouchableWithoutFeedback>
                )}
            </View>
        </View>
        {bottomContainer && (
            <View style={s.bottomContainer}>
                {bottomContainer}
            </View>
        )}
    </View>
);

const s = StyleSheet.create({
    container: {
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        shadowColor: '#E9EAF5',
        shadowOffset: {width: 0, height: 7},
        shadowOpacity: 0.30,
        shadowRadius: 10,
        elevation: .5,
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: sizes.horizontalOffset,
    },
    bottomContainer: {
        paddingHorizontal: sizes.horizontalOffset,
        paddingTop: 10,
        paddingBottom: 8
    },
    headerButton: {
        fontSize: fontSize(17),
        color: '#576090'
    },
    text: {
        fontSize: fontSize(17),
        fontWeight: '600',
        color: '#323B45',
        letterSpacing: -0.41,
        textAlign: 'center'
    },
    left: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    center: {
        flex: 2.5,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    right: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});
